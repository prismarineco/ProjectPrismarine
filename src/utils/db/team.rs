#![allow(dead_code)]
use crate::utils::db::Player;
use crate::utils::misc;
use crate::PoolConn;
use chrono::{DateTime, TimeZone, Utc};
use std::backtrace::Backtrace;
use unicode_segmentation::UnicodeSegmentation;

#[derive(Debug, Clone)]
pub struct Team {
    /// Name of the team.
    name: String,
    /// List of the team's players.
    players: Vec<Player>,
    /// This is the *only* person allowed to modify the team. *ALWAYS
    /// MAKE SURE THAT TEAM MODIFICATION COMMANDS VALIDATE FOR CAPTAIN
    /// STATUS!*
    captain: Player,

    /// Discord embeds have a cap, so we'll need to make sure that we
    /// don't get ridiculously long descriptions. If someone *wants*
    /// to have a ridiculously long description, use a Google Doc and
    /// link it in this field.
    description: Option<String>,

    /// This is meant to be a URL, may need to find a way to make sure
    /// passed in URLs are valid
    thumbnail: Option<String>,

    /// Time when team was registered into the bot. This is immutable
    /// and set upon creation.
    creation_time: DateTime<Utc>,

    /// Time (if a team is going to be deleted) when a team is going to
    /// be deleted.
    deletion_time: Option<DateTime<Utc>>,

    /// Whether or not a team is recruiting.
    pub recruiting: bool,

    /// The tournaments a team has participated. Only the most recent
    /// 3 will be shown, but any registered in the bot will be stored.
    tournaments: Vec<Tournament>,
}

#[derive(Debug, Clone)]
pub struct Tournament {
    /// The name of the tournament.
    name: String,
    /// How the team registering this tourney result placed.
    place: i16,
    /// When this result was registered into the bot. This
    /// is stored in the db as a UNIX timestamp.
    time: DateTime<Utc>,
}

#[derive(Debug)]
pub struct Invite {
    /// The player that the invite was sent to
    recipient: Player,
    /// The team that sent it
    sender: Team,
    /// whatever message the team included
    message: Option<String>,
    /// time this invite expires. Set to 3 days after sending
    deletion_time: DateTime<Utc>,
}

impl Tournament {
    pub fn new(name: String, place: i16, time: DateTime<Utc>) -> Self {
        Tournament { name, place, time }
    }
    pub fn name(&self) -> &String {
        &self.name
    }
    pub fn place(&self) -> &i16 {
        &self.place
    }
    pub fn time(&self) -> &DateTime<Utc> {
        &self.time
    }
}

/// Here's how the invite system is going to work.
///
///1. The captain of a team runs some command to send off an invite to a user, say `prx.team
///   invite @foobar` or something along those lines.
///
///2. The command frontend will verify that that user is:
///
///    a. registered as a free agent.
///
///    b. Not currently on a team, in which case an invite would be pointless and the captain
///    should ask the user to drop their old team before recieving an invite. This is to make
///    sure teams don't get accidentally overwritten. (By means of this process, we'll already
///    know if a user has a profile or not, and act accordingly.)
///
///    c. is an actual user, probably by means of the Discord API.
///
///   Once we know these things we can proceed to...
///
///3. Notify both entities and register the invite.
///
///    Once all is in place, a few things will be done:
///
///   1. The invite will be registered into the database, so that both a captain can recind the invite if necessary, and so that an invited user can see and accept the invite.
///
///   2. Both the captain and the user will be notified of the sent invite. The captain will just get a message in the channel they invoked the command in, while the recipient player will get a DM notifying them of the invite.
///
///4. One of two things will happen from now on, depending on if the invite is accepted or
///   not:
///
///   - if the invite is declined:
///
///     1. The invite is dropped from the database. The captain won't be notified, but the recipient will be.
///
///   - if the invite is accepted:
///
///     1. The recipient is registered is a part of the team with a call to [`Team::add_player`].
///        This will trigger subsequent a subsequent call to [`Player::set_team_id`].
///
///     2. Both the captain and the new team member will be notified of the new addition. From
///        there, the bot will recognize the new member as part of the team, and will be treated as
///        such with any further team command invocations.
///
/// [`Player::set_team_id`]: ./../player/struct.Player.html#method.set_team_id
/// [`Team::add_player`]: ./struct.Team.html#method.add_player
impl Invite {
    pub async fn add_to_db(
        conn: &mut PoolConn,
        recipient: &Player,
        sender: &Team,
        message: &Option<String>,
        deletion_time: DateTime<Utc>,
    ) -> Result<u64, sqlx::error::Error> {
        sqlx::query!(
            "
            INSERT INTO prismarine_rusted.invites(recipient, sender, invite_text, deletion_time)
            VALUES (?, ?, ?, ?);
            ",
            recipient.id(),
            sender.captain.id(),
            message,
            deletion_time.timestamp(),
        )
        .execute(conn)
        .await
    }

    pub async fn fetch_by_player(
        conn: &mut PoolConn,
        recipient: &Player,
    ) -> Result<Vec<Self>, misc::ModelError> {
        let mut set = sqlx::query!(
            "
            SELECT recipient, sender, invite_text, deletion_time FROM prismarine_rusted.invites WHERE recipient = ?;
            ", recipient.id()
        ).fetch_all(&mut *conn).await?;

        let mut invites: Vec<Self> = Vec::new();
        for row in &mut set {
            let recipient = (*recipient).clone();
            let sender = Team::from_db(&mut *conn, row.sender).await?;
            let deletion_time = Utc.timestamp(row.deletion_time as i64, 0);

            invites.push(Self {
                recipient,
                sender,
                message: row.invite_text.take(),
                deletion_time,
            });
        }
        Ok(invites)
    }

    pub async fn fetch_by_team(
        conn: &mut PoolConn,
        sender: &Team,
    ) -> Result<Vec<Self>, misc::ModelError> {
        let mut set = sqlx::query!(
            "
            SELECT recipient, sender, invite_text, deletion_time FROM prismarine_rusted.invites WHERE sender = ?;
            ", sender.id()
        ).fetch_all(&mut *conn).await?;

        let mut invites: Vec<Self> = Vec::new();
        for row in &mut set {
            let recipient = Player::from_db(&mut *conn, row.recipient).await?;
            let sender = (*sender).clone();
            let deletion_time = Utc.timestamp(row.deletion_time as i64, 0);

            invites.push(Self {
                recipient,
                sender,
                message: row.invite_text.take(),
                deletion_time,
            });
        }
        Ok(invites)
    }
    /// Delete an invite from the database.
    pub async fn delete(self, conn: &mut PoolConn) -> Result<(), sqlx::error::Error> {
        sqlx::query!(
            "DELETE FROM prismarine_rusted.invites WHERE recipient = ? AND sender = ?",
            self.recipient.id(),
            self.sender.id()
        )
        .execute(conn)
        .await?;
        Ok(())
    }

    /// Accept an invite from a team, updating the recipient's profile and deleting the invite at
    /// the same time.
    pub async fn accept(mut self, conn: &mut PoolConn) -> Result<(), misc::ModelError> {
        self.sender.add_player(&mut self.recipient)?;
        self.sender.update(&mut *conn).await?;
        self.delete(&mut *conn).await?;
        Ok(())
    }

    pub fn recipient(&self) -> &Player {
        &self.recipient
    }
    pub fn sender(&self) -> &Team {
        &self.sender
    }
    pub fn msg(&self) -> &Option<String> {
        &self.message
    }
    pub fn deletion_time(&self) -> &DateTime<Utc> {
        &self.deletion_time
    }
}

impl Team {
    /// Insert a new team into the database table.
    pub async fn add_to_db(
        conn: &mut PoolConn,
        new_cap: &mut Player,
        name: &str,
    ) -> Result<(), misc::ModelError> {
        sqlx::query!(
            "
            INSERT IGNORE INTO prismarine_rusted.team_profiles(captain, creation_time, name) VALUES (?, ?, ?);
            ", new_cap.id(), Utc::now().timestamp(), name
            ).execute(&mut *conn).await?;
        new_cap.set_team_id(Some(*new_cap.id()));
        new_cap.update(&mut *conn).await?;
        Ok(())
    }
    /// Get a team's data from the database. If not found, it'll return
    /// `ModelError::Team` with the id of the team that wasn't found. I *may*
    /// add an ability to search for teams by name, but this is easier for now.
    pub async fn from_db(conn: &mut PoolConn, team_id: u64) -> Result<Team, misc::ModelError> {
        // TODO: Implement this
        /* NOTE: There are several things that need to be done here.
        First, get the team's data out of the database. This is
        the key to all the other data. Next, get the team's captain
        for display. After which we can assemble the the vector of
        players by calling out to the `Player::from_db` function.
        ( Maybe it would be a good idea to make a macro that can
        reduce the needed boilerplate to get a player... something
        to consider for the future. ) Anyway, the last bit will be
        to query the database for the most recent tournaments the
        team has registered. Because we're storing the register
        times as unix timestamps, we *should* be able to just
        query with the "ORDER BY time DESC LIMIT 3" restriction.
        OH, and we may want to return a special error if a team
        is in the process of being deleted. That or have a special
        marker be displayed on the frontend to indicate that the
         team will be deleted soon. */

        // First thing's first: grab the team's captain. Their ID
        // is the same as their team's ID, so we can just use this.
        let captain = Player::from_db(&mut *conn, team_id).await?;

        // Next part is slightly harder. We need to get all the
        // players in a team. Since the team ID is an FK to the
        // captain's user ID, we can just query the player table w/
        // the captain's ID.
        let mut player_ids: Vec<u64> = Vec::new();

        for row in sqlx::query!(
            "
            SELECT id FROM prismarine_rusted.player_profiles WHERE team_id = ?;
            ",
            team_id
        )
        .fetch_all(&mut *conn)
        .await?
        {
            player_ids.push(row.id);
        }

        let mut players: Vec<Player> = Vec::new();

        for id in &player_ids {
            players.push(Player::from_db(&mut *conn, *id).await?);
        }

        // Next up, the tournaments. This should be as easy as gathering
        // the data and assembling the structs.

        let mut tournaments: Vec<Tournament> = Vec::new();

        for row in sqlx::query!(
            "
            SELECT name, place, time FROM prismarine_rusted.tournaments WHERE team = ?;
            ",
            team_id
        )
        .fetch_all(&mut *conn)
        .await?
        {
            tournaments.push(Tournament {
                name: row.name,
                place: row.place,
                time: Utc.timestamp(row.time, 0),
            })
        }

        // Now that we have everything else we need, it's time to get the
        // data for the team itself!

        let res = sqlx::query!(
            "
            SELECT name, description, thumbnail, creation_time, recruiting,
            deletion_time FROM prismarine_rusted.team_profiles WHERE captain = ?;
            ",
            team_id
        )
        .fetch_optional(&mut *conn)
        .await?;
        if let Some(dat) = res {
            Ok(Self {
                name: dat.name,
                players,
                captain,
                description: dat.description,
                thumbnail: dat.thumbnail,
                creation_time: Utc.timestamp(dat.creation_time, 0),
                recruiting: dat.recruiting != 0,
                deletion_time: if let Some(v) = dat.deletion_time {
                    Some(Utc.timestamp(v, 0))
                } else {
                    None
                },
                tournaments,
            })
        } else {
            Err(misc::ModelError::NotFound(
                misc::NFKind::Team(team_id),
                Backtrace::capture(),
            ))
        }
    }

    /// Get a team's name, and *only* their name. Don't need to be overfetching here, useful for
    /// profiles.
    pub async fn name_only(
        conn: &mut PoolConn,
        id: u64,
    ) -> Result<Option<String>, sqlx::error::Error> {
        sqlx::query!(
            "SELECT name FROM prismarine_rusted.team_profiles WHERE captain = ?",
            id
        )
        .fetch_optional(conn)
        .await
        .map(|x| if let Some(y) = x { Some(y.name) } else { None })
    }

    pub fn captain(&self) -> &Player {
        &self.captain
    }

    /// Get a team's name.
    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn id(&self) -> &u64 {
        self.captain.id()
    }

    /// Set a team's name. NOTE: We'll probably have to put
    /// a cap on name lengths if we get any issues. For now,
    /// I'll keep it uncapped, but I'll be sure to monitor
    /// for any issues.
    pub fn set_name(&mut self, new_name: String) {
        self.name = new_name;
    }

    /// Get the players from a team.
    pub fn players(&self) -> &Vec<Player> {
        &self.players
    }
    pub fn add_player(
        &mut self,
        player: &mut Player,
    ) -> Result<(), crate::utils::misc::ModelError> {
        if player.team_id().is_some() {
            return Err(crate::utils::misc::ModelError::PlayerInTeam);
        } else if self.del_time().is_some() {
            return Err(crate::utils::misc::ModelError::DeletionInProgress);
        }
        self.players.push(player.clone());
        Ok(())
    }
    /// Get a team's description.
    pub fn desc(&self) -> &Option<String> {
        &self.description
    }
    /// Replace a team's description with a brand new one.
    pub fn mod_desc(&mut self, new: Option<String>) -> Result<(), ()> {
        if UnicodeSegmentation::graphemes(
            (&new).as_ref().cloned().unwrap_or_default().as_str(),
            true,
        )
        .count()
            > 2048
        {
            return Err(());
        } else {
            self.description = new;
        }
        Ok(())
    }
    /// Get a team's thumbnail.
    pub fn thumbnail(&self) -> &Option<String> {
        &self.thumbnail
    }
    /// Set a team's thumbnail.
    pub fn set_thumbnail(&mut self, url: Option<String>) {
        self.thumbnail = url;
    }
    pub fn time_created(&self) -> &DateTime<Utc> {
        &self.creation_time
    }
    pub fn del_time(&self) -> &Option<DateTime<Utc>> {
        &self.deletion_time
    }
    pub fn set_del_time(&mut self, should_del: bool, time: Option<i64>) {
        if should_del {
            self.deletion_time =
                Some(Utc::now() + time::Duration::days(if let Some(v) = time { v } else { 7 }));
        } else {
            self.deletion_time = None;
        }
    }
    pub fn tournaments(&self) -> &Vec<Tournament> {
        &self.tournaments
    }
    pub fn add_tournament(&mut self, tournament: Tournament) {
        self.tournaments.push(tournament);
    }

    pub async fn update(&mut self, conn: &mut PoolConn) -> Result<(), misc::ModelError> {
        let dt = if let Some(v) = self.deletion_time {
            Some(v.timestamp())
        } else {
            None
        };

        sqlx::query!(
            "
            UPDATE prismarine_rusted.team_profiles
                SET name = ?,
                deletion_time = ?,
                description = ?,
                thumbnail = ?,
                recruiting = ?
            WHERE captain = ?;
            ",
            &self.name,
            dt,
            &self.description,
            &self.thumbnail,
            self.recruiting,
            self.captain.id(),
        )
        .execute(&mut *conn)
        .await?;

        for player in &mut self.players {
            player.set_team_id(Some(*self.captain.id()));
            player.set_free_agent(false);
            player.update(&mut *conn).await?;
        }

        for tourney in &self.tournaments {
            sqlx::query!(
                "
        INSERT INTO prismarine_rusted.tournaments(name, place, time, team)
            VALUES (?, ?, ?, ?);
            ",
                &tourney.name,
                &tourney.place,
                &tourney.time.timestamp(),
                &self.captain.id(),
            )
            .execute(&mut *conn)
            .await?;
        }

        Ok(())
    }
}
