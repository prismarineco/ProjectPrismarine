#![cfg(test)]
use crate::utils::db::Player;
use crate::utils::db::{loadout::RawLoadoutData, Loadout};
use crate::utils::db::{Invite, Team, Tournament};
use crate::utils::misc::ModelError;
use crate::Prisbot;
use chrono::Utc;
use std::io::Read;

async fn conn() -> crate::PoolConn {
    let mut f = String::new();
    std::fs::File::open("prisbot.toml")
        .expect("Expected config file named 'prisbot.toml'")
        .read_to_string(&mut f)
        .unwrap();
    let conf = {
        let p: Prisbot = toml::from_str(&f).expect("Invalid TOML config");
        p.prisbot
    };

    sqlx::MySqlPool::new(&conf.database.url)
        .await
        .unwrap()
        .acquire()
        .await
        .unwrap()
}

#[tokio::test]
async fn player_add() {
    Player::add_to_db(&mut conn().await, 1).await.unwrap();
    Player::add_to_db(&mut conn().await, 2).await.unwrap();
    Player::add_to_db(&mut conn().await, 3).await.unwrap();
    Player::add_to_db(&mut conn().await, 4).await.unwrap();
}

#[tokio::test]
async fn player_from() {
    Player::from_db(&mut conn().await, 1).await.unwrap();
}

#[tokio::test]
async fn player_update() {
    let mut player = Player::from_db(&mut conn().await, 1).await.unwrap();
    player.level = 42;
    player.update(&mut conn().await).await.unwrap();
}

#[test]
fn loadout_raw_deserialize() {
    // NOTE: Run this test w/ `--nocapture` to see the output
    let test_str = "080311694ac62098ce6e214e5";
    let out = RawLoadoutData::parse(test_str);
    println!("{:#?}", out);
    // Output should look something like this (the JSON from the Python implementation,
    // pretty printed):
    // {'clothes': {'gear_id': 98, 'main': 1, 'subs': [6, 6, 14]},
    // 'head': {'gear_id': 17, 'main': 13, 'subs': [5, 5, 12]},
    // 'id': 3,
    // 'set': 8,
    // 'shoes': {'gear_id': 110, 'main': 4, 'subs': [5, 7, 5]}}
    if let Err(e) = out {
        panic!("Deserialization Failed {:?}", e)
    }
    println!(
        "{}",
        serde_json::to_string_pretty(&(out.unwrap()))
            .unwrap()
            .as_str()
    );
}

#[tokio::test]
async fn player_re_fc() {
    let mut player = Player::from_db(&mut conn().await, 1).await.unwrap();
    // Test Cases:
    //a
    //k
    //0000-0000-0000
    //123412341234
    //1234t1234t1234t
    //aoeuaoeuaoeu12348888aaaa888a8
    //1234-1234-122-3
    //12341234123
    //412341299999
    //0 0 0 0 0 0 0 0 0 0 0 9
    //' or 1=1 #
    //1234 1234 4311
    player.set_fc("a").unwrap_err();
    player.set_fc("k").unwrap_err();
    player.set_fc("0000-0000-0000").unwrap();
    player.set_fc("123412341234").unwrap();
    player.set_fc("1234t1234t1234t").unwrap();
    player.set_fc("aoeuaoeuaoeu12348888aaaa888a8").unwrap();
    player.set_fc("1234-1234-122-3").unwrap();
    player.set_fc("12341234123").unwrap_err();
    player.set_fc("412341299999").unwrap();
    player.set_fc("0 0 0 0 0 0 0 0 0 0 0 9").unwrap();
    player.set_fc("' or 1=1 #").unwrap_err();
    player.set_fc("1234 1234 4311").unwrap();
}

#[tokio::test]
async fn player_pos_setting() {
    let mut player = Player::from_db(&mut conn().await, 1).await.unwrap();
    player.set_pos(0_i16).unwrap();
    player.set_pos(1_i16).unwrap();
    player.set_pos(2_i16).unwrap();
    player.set_pos(3_i16).unwrap();
    player.set_pos(4_i16).unwrap();
    player.set_pos(5_i16).unwrap_err();
}

#[tokio::test]
async fn player_rank_setting() {
    let mut player = Player::from_db(&mut conn().await, 1).await.unwrap();
    let test_cases = vec![
        // A set of 'possible' rank test cases.
        "C-", "C", "C+", "b-", "b", "b+", "A-", "A", "A+", "S", "S+", "S+3", "S+7", "X", "X 2500",
        "X 30000", "X2000",
    ];
    let mut failed_cases: Vec<(&&str, ModelError)> = Vec::new();

    for case in &test_cases {
        match player.set_rank(&"sz".to_string(), &(*case).to_string()) {
            Ok(_) => (),
            Err(e) => failed_cases.push((case, e)),
        }
    }
    println!("{:#?}", failed_cases);
}

#[tokio::test]
async fn loadout_full_deserial() {
    let test_ld = "0000000000000000000000000";
    let raw = RawLoadoutData::parse(test_ld).unwrap();
    println!(
        "{:#?}",
        Loadout::from_raw(&mut conn().await, raw).await.unwrap()
    );
}

#[tokio::test]
async fn loadout_image_gen() {
    let test_ld = "080311694ac62098ce6e214e5";
    let raw = RawLoadoutData::parse(test_ld).unwrap();
    let ld = Loadout::from_raw(&mut conn().await, raw).await.unwrap();
    ld.to_img().unwrap().save("ld_test.png").unwrap();
}

#[tokio::test]
async fn team_add() {
    Team::add_to_db(
        &mut conn().await,
        &Player::from_db(&mut conn().await, 1).await.unwrap(),
        "foobar",
    )
    .await
    .unwrap();
}

#[tokio::test]
async fn team_from() {
    Team::from_db(&mut conn().await, 1).await.unwrap();
}

#[tokio::test]
async fn team_update() {
    let mut team = Team::from_db(&mut conn().await, 1).await.unwrap();
    team.mod_desc(Some("baz bah bur".to_string())).unwrap();
    team.add_tournament(Tournament::new("footourney".to_string(), 5_i16, Utc::now()));

    team.update(&mut conn().await).await.unwrap();
}

#[tokio::test]
async fn invite_add() {
    let team = Team::from_db(&mut conn().await, 1).await.unwrap();
    let player = Player::from_db(&mut conn().await, 2).await.unwrap();
    Invite::add_to_db(
        &mut conn().await,
        &player,
        &team,
        &None,
        Utc::now() + time::Duration::days(3),
    )
    .await
    .unwrap();
}

#[tokio::test]
async fn invite_from() {
    println!(
        "{:#?}",
        Invite::from_db(&mut conn().await, 2).await.unwrap()
    );
}
