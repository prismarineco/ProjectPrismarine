use crate::impl_string_utils;
use crate::utils::misc;
use crate::utils::misc::{ModelError, NFKind};
use crate::PoolConn;
use image::{imageops::overlay as paste, DynamicImage, ImageResult};
use serde::{Deserialize, Serialize};
use std::backtrace::Backtrace;
use std::collections::HashMap;

impl_string_utils!();

static LOADOUT_BASE_PATH: &str = "assets/img/loadout_base.png";

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Loadout {
    raw: RawLoadoutData,
    head: GearItem,
    clothes: GearItem,
    shoes: GearItem,
    main_wep: MainWeapon,
    sub_wep: SubWeapon,
    special_wep: SpecialWeapon,
}

impl Loadout {
    pub async fn from_raw(conn: &mut PoolConn, raw: RawLoadoutData) -> Result<Loadout, ModelError> {
        let head = GearItem::from_raw(&mut *conn, &raw.head, "head").await?;
        let clothes = GearItem::from_raw(&mut *conn, &raw.clothes, "clothes").await?;
        let shoes = GearItem::from_raw(&mut *conn, &raw.shoes, "shoes").await?;

        let kit = MainWeapon::from_raw(&mut *conn, &raw).await?;
        let sub = kit.clone().sub;
        let special = kit.clone().special;

        Ok(Loadout {
            raw,
            head,
            clothes,
            shoes,
            main_wep: kit,
            sub_wep: sub,
            special_wep: special,
        })
    }
    #[allow(clippy::too_many_lines)] // TODO: Deal with this later
    pub fn to_img(&self) -> ImageResult<DynamicImage> {
        let mut base = match image::open(LOADOUT_BASE_PATH) {
            Ok(v) => v,
            Err(e) => return Err(e),
        };
        let coords = misc::GearCoords::get();
        {
            // Headgear pasting
            let gear_coords = if let Some(v) = coords.get("head") {
                v
            } else {
                unreachable!()
            };
            let gear_img = image::open(self.head.image.clone()).unwrap().resize(
                96,
                96,
                image::FilterType::Lanczos3,
            );
            let sub_imgs = {
                let mut vec: Vec<DynamicImage> = Vec::new();
                for sub in &self.head.subs {
                    vec.push(match sub {
                        None => image::open("assets/img/skills/Unknown.png")
                            .unwrap()
                            .resize(24, 24, image::FilterType::Lanczos3),
                        Some(v) => image::open(&v.image).unwrap().resize(
                            24,
                            24,
                            image::FilterType::Lanczos3,
                        ),
                    });
                }
                vec
            };
            let sub_imgs = sub_imgs.iter();
            let main_img = match &self.head.main {
                Some(v) => {
                    image::open(&v.image)
                        .unwrap()
                        .resize(32, 32, image::FilterType::Lanczos3)
                }
                &None => image::open("assets/img/skills/Unknown.png")
                    .unwrap()
                    .resize(32, 32, image::FilterType::Lanczos3),
            };
            for sub in sub_imgs.zip(gear_coords.subs.iter()) {
                paste(&mut base, sub.0, (sub.1).0, (sub.1).1);
            }
            paste(&mut base, &main_img, gear_coords.main.0, gear_coords.main.1);
            paste(&mut base, &gear_img, gear_coords.gear.0, gear_coords.gear.1);
        }
        {
            // Clothing pasting
            let gear_coords = if let Some(v) = coords.get("clothes") {
                v
            } else {
                unreachable!()
            };
            let gear_img = image::open(self.clothes.image.clone()).unwrap().resize(
                96,
                96,
                image::FilterType::Lanczos3,
            );
            let sub_imgs = {
                let mut vec: Vec<DynamicImage> = Vec::new();
                for sub in &self.clothes.subs {
                    vec.push(match sub {
                        None => image::open("assets/img/skills/Unknown.png")
                            .unwrap()
                            .resize(24, 24, image::FilterType::Lanczos3),
                        Some(v) => image::open(&v.image).unwrap().resize(
                            24,
                            24,
                            image::FilterType::Lanczos3,
                        ),
                    });
                }
                vec
            };
            let sub_imgs = sub_imgs.iter();
            let main_img = match &self.clothes.main {
                Some(v) => {
                    image::open(&v.image)
                        .unwrap()
                        .resize(32, 32, image::FilterType::Lanczos3)
                }
                &None => image::open("assets/img/skills/Unknown.png")
                    .unwrap()
                    .resize(32, 32, image::FilterType::Lanczos3),
            };
            for sub in sub_imgs.zip(gear_coords.subs.iter()) {
                paste(&mut base, sub.0, (sub.1).0, (sub.1).1);
            }
            paste(&mut base, &main_img, gear_coords.main.0, gear_coords.main.1);
            paste(&mut base, &gear_img, gear_coords.gear.0, gear_coords.gear.1);
        }
        {
            // Shoe pasting
            let gear_coords = if let Some(v) = coords.get("shoes") {
                v
            } else {
                unreachable!()
            };
            let gear_img = image::open(self.shoes.image.clone()).unwrap().resize(
                96,
                96,
                image::FilterType::Lanczos3,
            );
            let sub_imgs = {
                let mut vec: Vec<DynamicImage> = Vec::new();
                for sub in &self.shoes.subs {
                    vec.push(match sub {
                        None => image::open("assets/img/skills/Unknown.png")
                            .unwrap()
                            .resize(24, 24, image::FilterType::Lanczos3),
                        Some(v) => image::open(&v.image).unwrap().resize(
                            24,
                            24,
                            image::FilterType::Lanczos3,
                        ),
                    });
                }
                vec
            };
            let sub_imgs = sub_imgs.iter();
            let main_img = match &self.shoes.main {
                Some(v) => {
                    image::open(&v.image)
                        .unwrap()
                        .resize(32, 32, image::FilterType::Lanczos3)
                }
                &None => image::open("assets/img/skills/Unknown.png")
                    .unwrap()
                    .resize(32, 32, image::FilterType::Lanczos3),
            };
            for sub in sub_imgs.zip(gear_coords.subs.iter()) {
                paste(&mut base, sub.0, (sub.1).0, (sub.1).1);
            }
            paste(&mut base, &main_img, gear_coords.main.0, gear_coords.main.1);
            paste(&mut base, &gear_img, gear_coords.gear.0, gear_coords.gear.1);
        }
        {
            // Weapon pasting
            let wep_coords = misc::WepCoords::get();
            let main_wep_img = image::open(&self.main_wep.image).unwrap().resize(
                96,
                96,
                image::FilterType::Lanczos3,
            );
            let sub_wep_img = image::open(&self.sub_wep.image).unwrap().resize(
                32,
                32,
                image::FilterType::Lanczos3,
            );
            let special_wep_img = image::open(&self.special_wep.image).unwrap().resize(
                32,
                32,
                image::FilterType::Lanczos3,
            );
            paste(
                &mut base,
                &main_wep_img,
                wep_coords.main.0,
                wep_coords.main.1,
            );
            paste(&mut base, &sub_wep_img, wep_coords.sub.0, wep_coords.sub.1);
            paste(
                &mut base,
                &special_wep_img,
                wep_coords.special.0,
                wep_coords.special.1,
            );
        }

        Ok(base)
    }
    pub fn raw(&self) -> &RawLoadoutData {
        &self.raw
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GearItem {
    id: u32,
    image: String,
    localized_name: HashMap<String, Option<String>>,
    main: Option<Ability>,
    name: String,
    splatnet: u16,
    stars: u8,
    subs: [Option<Ability>; 3],
}

// Use local query result struct to act as an absraction across the 'different' records
// that `sqlx::query!` returns
struct QueryResult {
    name: String,
    image: String,
    localized_name: String,
    stars: u8,
    splatnet: u16,
}

macro_rules! check_if_exists {
    ($query:expr, $raw:expr) => {
        match $query {
            Ok(v) => v,
            Err(sqlx::error::Error::RowNotFound) => {
                return Err(ModelError::NotFound(
                    NFKind::GearItem($raw.gear_id),
                    Backtrace::capture(),
                ))
            }
            Err(e) => Err(e)?,
        }
    };
}

impl GearItem {
    pub async fn from_raw(
        conn: &mut PoolConn,
        raw: &RawGearItem,
        gear_type: &str,
    ) -> Result<GearItem, ModelError> {
        let res = match gear_type {
            "head" => {
                let dat = check_if_exists!(sqlx::query!(
                    "SELECT name, image, localized_name, stars, splatnet FROM prismarine_rusted.headgear WHERE id = ? LIMIT 1;",
                    raw.gear_id
                )
                .fetch_one(&mut *conn)
                .await, raw);
                QueryResult {
                    name: dat.name,
                    image: dat.image,
                    localized_name: dat.localized_name,
                    stars: dat.stars,
                    splatnet: dat.splatnet,
                }
            }
            "clothes" => {
                let dat = check_if_exists!(sqlx::query!(
                    "SELECT name, image, localized_name, stars, splatnet FROM prismarine_rusted.clothing WHERE id = ? LIMIT 1;",
                    raw.gear_id
                )
                .fetch_one(&mut *conn)
                .await, raw);
                QueryResult {
                    name: dat.name,
                    image: dat.image,
                    localized_name: dat.localized_name,
                    stars: dat.stars,
                    splatnet: dat.splatnet,
                }
            }
            "shoes" => {
                let dat = check_if_exists!(sqlx::query!(
                    "SELECT name, image, localized_name, stars, splatnet FROM prismarine_rusted.shoes WHERE id = ? LIMIT 1;",
                    raw.gear_id
                )
                .fetch_one(&mut *conn)
                .await, raw);
                QueryResult {
                    name: dat.name,
                    image: dat.image,
                    localized_name: dat.localized_name,
                    stars: dat.stars,
                    splatnet: dat.splatnet,
                }
            }
            _ => unreachable!(),
        };

        let mut subs: [Option<Ability>; 3] = [None, None, None];
        for (idx, sub_id) in raw.subs.iter().enumerate() {
            subs[idx] = Ability::from_db(&mut *conn, *sub_id).await?;
        }
        let local: HashMap<String, Option<String>> = match serde_json::from_str(&res.localized_name)
        {
            Ok(v) => v,
            Err(e) => return Err(ModelError::Unknown(e.to_string(), Backtrace::capture())),
        };

        Ok(GearItem {
            id: raw.gear_id,
            image: res.image,
            localized_name: local,
            main: Ability::from_db(conn, raw.main).await?,
            name: res.name,
            splatnet: res.splatnet,
            stars: res.stars,
            subs,
        })
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RawLoadoutData {
    hex: String,
    id: u32,
    set: u32,
    head: RawGearItem,
    clothes: RawGearItem,
    shoes: RawGearItem,
}

impl RawLoadoutData {
    /// Deserialize a raw base-16 encoded string into a `RawLoadout` struct
    /// ## Arguments
    /// * `dat`: The base-16 encoded string you want to deserialize. The function will verify if
    /// the string is valid before conversion.
    /// ## Return Value:
    /// * `Result<RawLoadout, ModelError>`: A result wrapping either a `RawLoadout` instance
    /// or the error that resulted.
    pub fn parse(dat: &str) -> Result<Self, ModelError> {
        if u32::from_str_radix(
            misc::hex_to_bin(&String::from(dat.slice(0..1)))
                .unwrap_or_else(|_| String::from("1"))
                .as_str(),
            2,
        )
        .unwrap()
            != 0
        {
            return Err(ModelError::InvalidParameter(
                "Invalid hex string".to_string(),
            ));
        }
        let set = u32::from_str_radix(
            misc::hex_to_bin(&String::from(dat.slice(1..2)))
                .unwrap_or_default()
                .as_str(),
            2,
        )
        .unwrap();
        let id = u32::from_str_radix(
            misc::hex_to_bin(&String::from(dat.slice(2..4)))
                .unwrap_or_default()
                .as_str(),
            2,
        )
        .unwrap();
        let head = RawGearItem::parse(String::from(dat.slice(4..11)).as_str()).unwrap();
        let clothes = RawGearItem::parse(String::from(dat.slice(11..18)).as_str()).unwrap();
        let shoes = RawGearItem::parse(String::from(dat.slice(18..25)).as_str()).unwrap();
        Ok(Self {
            hex: dat.to_string(),
            id,
            set,
            head,
            clothes,
            shoes,
        })
    }
    pub fn hex(&self) -> &String {
        &self.hex
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct RawGearItem {
    gear_id: u32,
    main: u32,
    subs: Vec<u32>,
}

impl RawGearItem {
    pub fn parse(dat: &str) -> Option<RawGearItem> {
        let gear_id = u32::from_str_radix(dat.slice(0..2), 16).unwrap();
        let bin_str = misc::hex_to_bin(&dat.slice(2..7).to_string()).unwrap();
        let mut subs: Vec<u32> = Vec::new();
        let main = u32::from_str_radix(bin_str.slice(0..5), 2).unwrap();
        subs.push(u32::from_str_radix(bin_str.slice(5..10), 2).unwrap());
        subs.push(u32::from_str_radix(bin_str.slice(10..15), 2).unwrap());
        subs.push(u32::from_str_radix(bin_str.slice(15..20), 2).unwrap());

        Some(RawGearItem {
            gear_id,
            main,
            subs,
        })
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Ability {
    id: u32,
    image: String,
    localized_name: HashMap<String, Option<String>>,
    name: String,
}

impl Ability {
    pub async fn from_db(conn: &mut PoolConn, id: u32) -> Result<Option<Ability>, ModelError> {
        let res = match sqlx::query!(
            "SELECT * FROM prismarine_rusted.abilities WHERE id = ? LIMIT 1;",
            id
        )
        .fetch_one(conn)
        .await
        {
            Ok(v) => v,
            Err(sqlx::error::Error::RowNotFound) => return Ok(None),
            Err(e) => return Err(e.into()),
        };

        let localized_name: HashMap<String, Option<String>> =
            match serde_json::from_str(&res.localized_name) {
                Ok(v) => v,
                Err(e) => return Err(ModelError::Unknown(e.to_string(), Backtrace::capture())),
            };

        Ok(Some(Ability {
            id,
            image: res.image,
            localized_name,
            name: res.name,
        }))
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MainWeapon {
    class: u8,
    site_id: u8,
    name: String,
    image: String,
    localized_name: HashMap<String, Option<String>>,
    special: SpecialWeapon,
    sub: SubWeapon,
}

impl MainWeapon {
    pub async fn from_raw(conn: &mut PoolConn, raw: &RawLoadoutData) -> Result<Self, ModelError> {
        match sqlx::query!(
            "SELECT * FROM prismarine_rusted.main_weapons WHERE site_id = ? AND class = ? LIMIT 1;",
            raw.id,
            raw.set
        )
        .fetch_one(&mut *conn)
        .await
        {
            Ok(result) => Ok(Self {
                class: result.class,
                site_id: result.site_id,
                name: result.name,
                image: result.image,
                localized_name: match serde_json::from_str(&result.localized_name) {
                    Ok(v) => v,
                    Err(e) => return Err(ModelError::Unknown(e.to_string(), Backtrace::capture())),
                },
                special: SpecialWeapon::from_db(&mut *conn, result.special).await?,
                sub: SubWeapon::from_db(&mut *conn, result.sub).await?,
            }),
            Err(sqlx::error::Error::RowNotFound) => Err(ModelError::NotFound(
                NFKind::MainWeapon {
                    id: raw.id,
                    set: raw.set,
                },
                Backtrace::capture(),
            )),
            Err(e) => Err(e)?,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SubWeapon {
    image: String,
    localized_name: HashMap<String, Option<String>>,
    name: String,
}

impl SubWeapon {
    pub async fn from_db(conn: &mut PoolConn, name: String) -> Result<SubWeapon, ModelError> {
        match sqlx::query!(
            "SELECT * FROM prismarine_rusted.sub_weapons WHERE name = ? LIMIT 1;",
            name
        )
        .fetch_one(&mut *conn)
        .await
        {
            Ok(result) => {
                let localized_name: HashMap<String, Option<String>> =
                    match serde_json::from_str(&result.localized_name) {
                        Err(e) => {
                            return Err(ModelError::Unknown(e.to_string(), Backtrace::capture()))
                        }
                        Ok(v) => v,
                    };
                Ok(Self {
                    image: result.image,
                    localized_name,
                    name: result.name,
                })
            }
            Err(e) => match e {
                sqlx::error::Error::RowNotFound => {
                    return Err(ModelError::NotFound(
                        NFKind::SubWeapon(name),
                        Backtrace::capture(),
                    ))
                }
                e => Err(e)?,
            },
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SpecialWeapon {
    image: String,
    localized_name: HashMap<String, Option<String>>,
    name: String,
}

impl SpecialWeapon {
    pub async fn from_db(conn: &mut PoolConn, name: String) -> Result<SpecialWeapon, ModelError> {
        match sqlx::query!(
            "SELECT * FROM prismarine_rusted.special_weapons WHERE name = ? LIMIT 1;",
            name
        )
        .fetch_one(&mut *conn)
        .await
        {
            Ok(result) => {
                let local: HashMap<String, Option<String>> =
                    match serde_json::from_str(&result.localized_name) {
                        Ok(v) => v,
                        Err(e) => {
                            return Err(ModelError::Unknown(e.to_string(), Backtrace::capture()))
                        }
                    };

                Ok(SpecialWeapon {
                    image: result.image,
                    localized_name: local,
                    name: result.name,
                })
            }
            Err(sqlx::error::Error::RowNotFound) => Err(ModelError::NotFound(
                NFKind::SpecialWeapon(name),
                Backtrace::capture(),
            )),
            Err(e) => Err(ModelError::Database(e, Backtrace::capture())),
        }
    }
}
