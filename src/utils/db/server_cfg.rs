#![allow(dead_code)]
//! Configuration for servers. Allows for owners to only whitelist certain channels, set prefixes,
//! etc.

pub async fn get_server_prefix(
    conn: &mut crate::PoolConn,
    snowflake: u64,
) -> Result<Option<String>, sqlx::error::Error> {
    let res = sqlx::query!(
        "SELECT prefix FROM prismarine_rusted.server_prefix WHERE id = ?",
        snowflake
    )
    .fetch_optional(conn)
    .await?;
    if let Some(rec) = res {
        Ok(rec.prefix)
    } else {
        Ok(None)
    }
}

pub async fn set_server_prefix(
    conn: &mut crate::PoolConn,
    snowflake: u64,
    prefix: Option<&String>,
) -> Result<(), sqlx::error::Error> {
    Ok(sqlx::query!("INSERT INTO prismarine_rusted.server_prefix(prefix, id) VALUES (?, ?) ON DUPLICATE KEY UPDATE prefix = ?", prefix, snowflake, prefix).execute(conn).await.map(|_| {})?)
}

pub async fn drop_server_prefix(
    conn: &mut crate::PoolConn,
    snowflake: u64,
) -> Result<(), sqlx::error::Error> {
    Ok(sqlx::query!(
        "DELETE FROM prismarine_rusted.server_prefix WHERE id = ?",
        snowflake
    )
    .execute(conn)
    .await
    .map(|_| {})?)
}

pub async fn whitelist_channel(
    conn: &mut crate::PoolConn,
    server: u64,
    channel: u64,
) -> Result<(), sqlx::error::Error> {
    Ok(sqlx::query!(
        "INSERT INTO prismarine_rusted.server_channel_whitelist(id, channel) VALUES (?, ?)",
        server,
        channel
    )
    .execute(conn)
    .await
    .map(|_| {})?)
}

pub async fn unwhitelist_channel(
    conn: &mut crate::PoolConn,
    server: u64,
    channel: u64,
) -> Result<(), sqlx::error::Error> {
    Ok(sqlx::query!(
        "DELETE FROM prismarine_rusted.server_channel_whitelist WHERE id =  ? AND channel = ?",
        server,
        channel
    )
    .execute(conn)
    .await
    .map(|_| {})?)
}

pub async fn get_channel_whitelist(
    conn: &mut crate::PoolConn,
    server: u64,
) -> Result<Vec<u64>, sqlx::error::Error> {
    Ok(sqlx::query!(
        "SELECT channel FROM prismarine_rusted.server_channel_whitelist WHERE id = ?",
        server
    )
    .fetch_all(conn)
    .await?
    .into_iter()
    .map(|x| x.channel)
    .collect())
}

pub async fn clear_whitelist(
    conn: &mut crate::PoolConn,
    server: u64,
) -> Result<(), sqlx::error::Error> {
    Ok(sqlx::query!(
        "DELETE FROM prismarine_rusted.server_channel_whitelist WHERE id = ?",
        server
    )
    .execute(conn)
    .await
    .map(|_| {})?)
}
