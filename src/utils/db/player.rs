use crate::impl_string_utils;
use crate::utils::db::{loadout::RawLoadoutData, Loadout};
use crate::utils::misc::{ModelError, NFKind};
use crate::PoolConn;
use regex::Regex;
use sqlx::error::Error;
use std::backtrace::Backtrace;
use std::collections::HashMap;
use std::convert::TryFrom;
use unicode_segmentation::UnicodeSegmentation;

impl_string_utils!();

lazy_static! {
    static ref FCRE: Regex = Regex::new(r"\D").unwrap();
    static ref RANKRE: Regex = Regex::new(r"(([ABC][-+ ]?)|(S\+[0-9]?)|S|(X [0-9]{0,4}))").unwrap();
}

static SR_RANK_ARRAY: [&str; 6] = [
    "intern",
    "apprentice",
    "part-timer",
    "go-getter",
    "overachiever",
    "profreshional",
];

#[derive(Debug, Clone)]
pub struct Player {
    id: u64,
    friend_code: String,
    ign: String,
    pub level: i32,
    sz: String,
    tc: String,
    rm: String,
    cb: String,
    sr: String,
    pub position: Position,
    loadout: Loadout,
    team_id: Option<u64>,
    free_agent: bool,
    pub is_private: bool,
}

#[derive(Copy, Clone, Debug)]
pub enum Position {
    NotSet = 0,
    Frontline = 1,
    Midline = 2,
    Backline = 3,
    Flex = 4,
}

impl std::fmt::Display for Position {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::NotSet => write!(f, "Not Set"),
            Self::Frontline => write!(f, "Frontline"),
            Self::Midline => write!(f, "Midline"),
            Self::Backline => write!(f, "Backline"),
            Self::Flex => write!(f, "Flex"),
        }
    }
}

impl TryFrom<i16> for Position {
    type Error = ModelError;

    fn try_from(val: i16) -> Result<Self, Self::Error> {
        match val {
            0 => Ok(Self::NotSet),
            1 => Ok(Self::Frontline),
            2 => Ok(Self::Midline),
            3 => Ok(Self::Backline),
            4 => Ok(Self::Flex),
            v => Err(ModelError::InvalidParameter(format!(
                "Invalid position: {}",
                v
            ))),
        }
    }
}

impl Player {
    pub async fn add_to_db(conn: &mut PoolConn, user_id: u64) -> Result<u64, Error> {
        sqlx::query!(
            "INSERT IGNORE INTO prismarine_rusted.player_profiles(id) VALUES (?)",
            user_id
        )
        .execute(conn)
        .await
    }
    pub async fn from_db(conn: &mut PoolConn, user_id: u64) -> Result<Player, ModelError> {
        let result = sqlx::query!(
            "
            SELECT
                level,
                loadout,
                id,
                friend_code,
                ign,
                sz,
                tc,
                rm,
                cb,
                sr,
                position,
                team_id,
                free_agent,
                is_private
            FROM prismarine_rusted.player_profiles
            WHERE id = ?;
            ",
            user_id
        )
        .fetch_optional(&mut *conn)
        .await?;

        let row = if let Some(v) = result {
            v
        } else {
            return Err(ModelError::NotFound(
                NFKind::Player(user_id),
                Backtrace::capture(),
            ));
        };

        let loadout = Loadout::from_raw(&mut *conn, RawLoadoutData::parse(&row.loadout)?).await?;

        Ok(Player {
            id: row.id,
            friend_code: row.friend_code,
            ign: row.ign,
            level: row.level,
            sz: row.sz,
            tc: row.tc,
            rm: row.rm,
            cb: row.cb,
            sr: row.sr,
            position: Position::try_from(row.position)?,
            loadout,
            team_id: row.team_id,
            free_agent: row.free_agent != 0,
            is_private: row.is_private != 0,
        })
    }
    pub fn id(&self) -> &u64 {
        &self.id
    }
    pub fn fc(&self) -> &String {
        &self.friend_code
    }
    pub fn set_fc(&mut self, fc_string: &str) -> Result<(), ()> {
        let regexed = FCRE.replace_all(fc_string, "");

        if regexed.len() == 12 {
            let f1 = regexed.substring(0, 4);
            let f2 = regexed.substring(4, 4);
            let f3 = regexed.substring(8, 4);
            self.friend_code = format!("SW-{}-{}-{}", f1, f2, f3);
        } else {
            return Err(());
        }

        Ok(())
    }
    pub fn name(&self) -> &String {
        &self.ign
    }
    pub fn set_name(&mut self, name: String) -> Result<(), ()> {
        let __name__ = UnicodeSegmentation::graphemes(name.as_str(), true).collect::<Vec<&str>>();

        if __name__.len() > 10 || __name__.is_empty() {
            return Err(());
        } else {
            self.ign = name;
        }
        Ok(())
    }
    pub fn ranks(&self) -> HashMap<&'static str, &String> {
        let mut hm = HashMap::new();
        hm.insert("Splat Zones", &self.sz);
        hm.insert("Tower Control", &self.tc);
        hm.insert("Rainmaker", &self.rm);
        hm.insert("Clam Blitz", &self.cb);
        hm.insert("Salmon Run", &self.sr);
        hm
    }
    pub fn set_rank(&mut self, mode: &str, rank: &str) -> Result<(), ModelError> {
        match mode {
            "sz" | "splat_zones" | "sz_rank" => {
                if !RANKRE.is_match(rank) {
                    return Err(ModelError::InvalidParameter(format!(
                        "Invalid Rank: {}",
                        rank
                    )));
                }
                self.sz = rank.to_ascii_uppercase()
            }
            "tc" | "tower_control" | "tc_rank" => {
                if !RANKRE.is_match(rank) {
                    return Err(ModelError::InvalidParameter(format!(
                        "Invalid Rank: {}",
                        rank
                    )));
                }
                self.tc = rank.to_ascii_uppercase()
            }
            "rm" | "rainmaker" | "rm_rank" => {
                if !RANKRE.is_match(rank) {
                    return Err(ModelError::InvalidParameter(format!(
                        "Invalid Rank: {}",
                        rank
                    )));
                }
                self.rm = rank.to_ascii_uppercase()
            }
            "cb" | "clam_blitz" | "cb_rank" => {
                if !RANKRE.is_match(rank) {
                    return Err(ModelError::InvalidParameter(format!(
                        "Invalid Rank: {}",
                        rank
                    )));
                }
                self.cb = rank.to_ascii_uppercase()
            }
            "sr" | "salmon_run" | "sr_rank" => {
                for static_rank in &SR_RANK_ARRAY {
                    if &(rank.to_ascii_lowercase()) == static_rank {
                        use heck::TitleCase;

                        self.sr = rank.to_title_case().replace(' ', "-");
                        return Ok(());
                    }
                }
                return Err(ModelError::InvalidParameter(format!(
                    "Invalid Salmon Run Rank: {}",
                    rank
                )));
            }
            _ => {
                return Err(ModelError::InvalidParameter(format!(
                    "Invalid Mode: {}",
                    mode
                )))
            }
        }
        Ok(())
    }

    pub fn loadout(&self) -> &Loadout {
        &self.loadout
    }

    pub async fn set_loadout(&mut self, conn: &mut PoolConn, hex: &str) -> Result<(), ModelError> {
        let raw = RawLoadoutData::parse(hex)?;
        match Loadout::from_raw(conn, raw).await {
            Ok(v) => self.loadout = v,
            Err(e) => return Err(e),
        }
        Ok(())
    }

    pub fn team_id(&self) -> &Option<u64> {
        &self.team_id
    }

    pub fn set_team_id(&mut self, team_id: Option<u64>) {
        self.team_id = team_id;
    }

    pub fn is_free_agent(&self) -> &bool {
        &self.free_agent
    }

    pub fn set_free_agent(&mut self, is_fa: bool) {
        self.free_agent = is_fa;
        if is_fa {
            self.team_id = None;
        }
    }

    pub async fn update(&self, conn: &mut PoolConn) -> Result<u64, Error> {
        sqlx::query!(
            "
UPDATE prismarine_rusted.player_profiles
SET
    friend_code = ?,
    ign = ?,
    level = ?,
    sz = ?,
    tc = ?,
    rm = ?,
    cb = ?,
    sr = ?,
    position = ?,
    loadout = ?,
    team_id = ?,
    is_private = ?,
    free_agent = ?
WHERE id = ?;
                ",
            &self.friend_code,
            &self.ign,
            self.level,
            &self.sz,
            &self.tc,
            &self.rm,
            &self.cb,
            &self.sr,
            self.position as i16,
            self.loadout.raw().hex(),
            self.team_id,
            self.is_private,
            self.free_agent,
            self.id
        )
        .execute(conn)
        .await
    }
}
