/// Various useful constants for use within the bot.
pub mod emoji {
    pub const ONE: &'static str = "1\u{fe0f}\u{20e3}";
    pub const TWO: &'static str = "2\u{fe0f}\u{20e3}";
    pub const THREE: &'static str = "3\u{fe0f}\u{20e3}";
    pub const FOUR: &'static str = "4\u{fe0f}\u{20e3}";
    pub const FIVE: &'static str = "5\u{fe0f}\u{20e3}";
    pub const SIX: &'static str = "6\u{fe0f}\u{20e3}";
    pub const SEVEN: &'static str = "7\u{fe0f}\u{20e3}";
    pub const EIGHT: &'static str = "8\u{fe0f}\u{20e3}";
    pub const STOP: &'static str = "\u{23f9}";
    pub const STRT_ITEM: &'static str = "\u{23ee}";
    pub const PREV_ITEM: &'static str = "\u{25c0}";
    pub const NEXT_ITEM: &'static str = "\u{25b6}";
    pub const LAST_ITEM: &'static str = "\u{23ed}";
    pub const CHECK_MARK: &'static str = "\u{2705}";
    pub const CROSS_MARK: &'static str = "\u{274c}";
    pub const REGIONAL_INDICATOR_Y: &'static str = "\u{1f1fe}";
    pub const REGIONAL_INDICATOR_N: &'static str = "\u{1f1f3}";
}

pub mod values {
    #[cfg(debug_assertions)] // set when in debug mode (no `--release` flag)
    pub const DEL_BUFFER_DAYS: i64 = 1;
    #[cfg(not(debug_assertions))] // set in release mode (compiled w/ `--release` flag)
    pub const DEL_BUFFER_DAYS: i64 = 7;
}
