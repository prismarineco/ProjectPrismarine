//! The core of Project Prismarine. This is the entry point of the bot and where the boilerplate of Serenity goes.
#![feature(backtrace)]
#![warn(clippy::pedantic)]
#![allow(clippy::cast_possible_wrap)]
#![allow(clippy::cast_sign_loss)]
#![allow(clippy::cast_possible_truncation)]
extern crate chrono;
extern crate dbl; // top.gg API Wrapper.
extern crate serde; // Serialization and deserialization of JSON from DB into structs
extern crate serde_json; // JSON support of serde // Time keeping library.
#[macro_use]
extern crate log; // logging crate
extern crate flexi_logger as flex; // log framework
extern crate log_panics;
extern crate regex;
extern crate sqlx; // Async database library.
#[macro_use]
extern crate lazy_static; // Set static variables at runtime.
extern crate heck; // Case conversion crate.
extern crate image; // Image editing library.
extern crate time; // used with chrono.
extern crate tokio;
extern crate toml; // TOML parsing, for the new config.

use dbl::Client as APIClient; // Used to update discordbots.org
use serenity::{
    // Library for Discord. The central library for this bot.
    client::bridge::gateway::ShardManager,
    framework::{standard::macros::group, StandardFramework},
    model::gateway::Ready,
    model::prelude::*,
    prelude::*,
};
use std::{collections::HashSet, io::Read, sync::Arc};
use tokio::sync::watch;

// Declare modules for use
mod modules;
mod utils;
// Import the commands to put them into their groups
use modules::meta::*;
use modules::player::*;
use modules::server_cfg::*;
use modules::sudo::*;
use modules::team::*;

// Various holders to be carried across modules.

type PoolConn = sqlx::pool::PoolConnection<sqlx::MySqlConnection>;

struct ShardManagerContainer;
impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}
struct DatabaseKey;
impl TypeMapKey for DatabaseKey {
    type Value = Arc<sqlx::MySqlPool>;
}
struct AttachChannelID;
impl TypeMapKey for AttachChannelID {
    type Value = u64;
}

struct Handler;

#[serenity::async_trait]
impl EventHandler for Handler {
    async fn ready(&self, _: Context, ready: Ready) {
        info!(
            "{}#{} is online!",
            ready.user.name, ready.user.discriminator
        );
    }
}

// Configuration structure
#[derive(serde::Deserialize)]
struct Prisbot {
    prisbot: Config,
}

#[derive(serde::Deserialize)]
struct Config {
    discord: DiscordCfg,
    database: Database,
    log: Option<String>,
}
#[derive(serde::Deserialize)]
struct DiscordCfg {
    bot_token: String,
    dbl_api_token: Option<String>,
    owners: Vec<u64>,
    prefix: String,
    attach_channel: u64,
}
#[derive(serde::Deserialize)]
struct Database {
    url: String,
}

/* Commands & Command Groups */

//Commands for this group:
//sudo info
//     logout
//     latency
//     user
//     update_stats
//     print_reactions <- just a sandbox command for experiments 'n testing.
#[group]
#[commands(info, logout, latency, user, print_reactions)]
#[prefix("sudo")]
#[owners_only]
struct SudoMod;

#[group]
#[commands(set_prefix, whitelist_channel, unwhitelist_channel)]
#[prefix("cfg")]
struct ServerCfgMod;

//Commands for this group:
//player new
//       show
//       invites
//       update
#[group]
#[commands(player_new, player_show, player_invites, player_update)]
#[prefixes("p", "player")]
struct PlayerMod;

//Commands for this group:
//team new
//     update
//     delete
//     invite
//     show
//     register
#[group]
#[commands(
    team_new,
    team_update,
    team_delete,
    team_invite,
    team_show,
    team_register_tournament
)]
#[prefixes("t", "team")]
struct TeamMod;

// Commands for this group:
// show_prefix
#[group]
#[commands(show_prefix)]
struct MiscMod;

/* End of commands */

#[serenity::framework::standard::macros::hook]
async fn check_whitelist(ctx: &Context, msg: &Message, _: &str) -> bool {
    let mut conn = get_pool_from_ctx!(ctx).acquire().await.unwrap();
    let server_id = if let Some(id) = msg.guild_id {
        id
    } else {
        return true;
    };
    let server_id = *server_id.as_u64();

    let server = msg.guild(ctx).await.unwrap();
    let server = server.read().await;
    let member = server.member(ctx, msg.author.id).await.unwrap();

    if member.permissions(ctx).await.unwrap().administrator() {
        // Admins should be able to bypass checks, no matter what.
        return true;
    }

    match crate::utils::db::server_cfg::get_channel_whitelist(&mut conn, server_id).await {
        Ok(v) if v.is_empty() => true,
        // Err on the side of caution, don't need server owners jumping down our throats because of
        // a bug...
        Ok(v) => v.contains(msg.channel_id.as_u64()),
        Err(e) => {
            error!(
                "Error occurred when getting whitelist for server #{}\n\nError: {:?}",
                server_id, e
            );
            false
        }
    }
}

#[tokio::main]
async fn main() {
    let mut f = String::new();
    std::fs::File::open("prisbot.toml")
        .expect("Expected config file named 'prisbot.toml'")
        .read_to_string(&mut f)
        .unwrap();
    let conf = {
        let p: Prisbot = toml::from_str(&f).expect("Invalid TOML config");
        p.prisbot
    };

    log_panics::init();

    // Set up logging framework
    {
        flex::Logger::with_env_or_str(&conf.log.clone().unwrap_or("info".to_string()))
            .log_to_file()
            .format_for_files(|w, now, record| {
                write!(
                    w,
                    "[{}] {} [{}] {}",
                    now.now().format("%Y-%m-%d %H:%M:%S%.6f %:z"),
                    record.level(),
                    record.module_path().unwrap_or("<unnamed>"),
                    &record.args()
                )
            })
            .format_for_stderr(|w, now, record| {
                use flex::style;
                let level = record.level();
                write!(
                    w,
                    "[{}] {} [{}] {}",
                    style(level, now.now().format("%Y-%m-%d %H:%M:%S%.6f %:z")),
                    style(level, record.level()),
                    record.module_path().unwrap_or("<unnamed>"),
                    style(level, &record.args())
                )
            })
            .duplicate_to_stderr(flex::Duplicate::Info)
            .start()
            .unwrap();
    }

    // Check if the db config was done correctly
    let pool = match sqlx::MySqlPool::new(conf.database.url.as_str()).await {
        Ok(v) => v,
        Err(e) => {
            error!("Failed to connect to database: {:?}", e);
            std::process::exit(1);
        }
    };

    info!("Config acquired!");

    let mut client = Client::new(&conf.discord.bot_token)
        .event_handler(Handler)
        .framework(StandardFramework::new()
                .configure(|c| {
                    c.allow_dm(false)
                        .owners({
                            let mut own = HashSet::new();
                            for id in &conf.discord.owners {
                                own.insert(serenity::model::id::UserId(*id));
                            }
                            own
                        })
                        .prefix(&conf.discord.prefix)
                        .dynamic_prefix(|ctx, msg| Box::pin(async move {
                            let mut conn = match get_pool_from_ctx!(ctx).acquire().await {
                                Ok(v) => v,
                                Err(e) => {
                                    error!("Failed to acquire pooled database connection when rettrieving dynamic prefix: {:?}", e);
                                    return None;
                                }
                            };
                            match crate::utils::db::server_cfg::get_server_prefix(&mut conn, *msg.guild_id.unwrap().as_u64()).await {
                                Ok(v) => v,
                                Err(e) => {
                                    error!("Failed to get server prefix due to error, defaulting to configured prefix.\n\nError:{:?}", e);
                                    None
                                }
                            }
                        }))
                })
                .before(check_whitelist)
                .group(&SUDOMOD_GROUP)
                .group(&SERVERCFGMOD_GROUP)
                .group(&PLAYERMOD_GROUP)
                .group(&TEAMMOD_GROUP)
                .group(&MISCMOD_GROUP)
                .help(&ASSIST)
        ).await.unwrap();

    let p2 = pool.clone();

    {
        let mut data = client.data.write().await;
        data.insert::<ShardManagerContainer>(Arc::clone(&client.shard_manager));
        data.insert::<DatabaseKey>(Arc::new(p2));
        data.insert::<AttachChannelID>(conf.discord.attach_channel);
    }

    let cache = client.cache_and_http.cache.read().await.clone();
    let manager = Arc::clone(&client.shard_manager);
    let http = client.cache_and_http.http.clone();

    let (transmitter, reciever) = watch::channel(false);

    let dbl_task = if let Some(token) = conf.discord.dbl_api_token {
        let api_client = APIClient::new(token).unwrap();
        let mut callback_stp = reciever.clone();
        tokio::task::spawn(async move {
            loop {
                match tokio::time::timeout(
                    chrono::Duration::seconds(60).to_std().unwrap(),
                    callback_stp.recv(),
                )
                .await
                {
                    Err(tokio::time::Elapsed { .. }) | Ok(None) | Ok(Some(false)) => (),
                    Ok(Some(true)) => break,
                }

                let runners = manager.lock().await;
                let runners = runners.runners.lock().await;

                let shard_count = (*runners).len();

                let server_count = cache.guilds.len();

                let stats = dbl::types::ShardStats::Cumulative {
                    shard_count: Some(shard_count as u64),
                    server_count: server_count as u64,
                };

                match api_client
                    .update_stats(*http.get_current_user().await.unwrap().id.as_u64(), stats)
                    .await
                {
                    Ok(_) => info!("Posted stats to discordbots.org!"),
                    Err(e) => error!("Failed to post stats to discordbots.org: {:#?}", e),
                }

                tokio::time::delay_for(tokio::time::Duration::from_secs(60)).await;
            }
        })
    } else {
        let mut callback_stp = reciever.clone();
        tokio::task::spawn(async move {
            loop {
                match tokio::time::timeout(
                    chrono::Duration::seconds(60).to_std().unwrap(),
                    callback_stp.recv(),
                )
                .await
                {
                    Err(tokio::time::Elapsed { .. }) | Ok(None) | Ok(Some(false)) => (),
                    Ok(Some(true)) => break,
                }
                tokio::task::yield_now().await;
            }
        })
    };

    let team_clean_task = {
        let mut callback_stp = reciever.clone();
        let mut conn = pool.acquire().await.unwrap();
        tokio::task::spawn(async move {
            loop {
                match tokio::time::timeout(
                    chrono::Duration::seconds(60).to_std().unwrap(),
                    callback_stp.recv(),
                )
                .await
                {
                    Err(tokio::time::Elapsed { .. }) | Ok(None) | Ok(Some(false)) => (),
                    Ok(Some(true)) => break,
                }

                let _ = sqlx::query!("DELETE FROM prismarine_rusted.team_profiles WHERE deletion_time IS NOT NULL AND deletion_time < ?", chrono::offset::Utc::now().timestamp())
                    .execute(&mut conn).await.map_err(|e| error!("Failure in database execution {:?}", e));

                tokio::time::delay_for(chrono::Duration::seconds(60).to_std().unwrap()).await;
            }
        })
    };
    let invite_clean_task = {
        let mut callback_stp = reciever.clone();
        let mut conn = pool.acquire().await.unwrap();
        tokio::task::spawn(async move {
            loop {
                match tokio::time::timeout(
                    chrono::Duration::seconds(60).to_std().unwrap(),
                    callback_stp.recv(),
                )
                .await
                {
                    Err(tokio::time::Elapsed { .. }) | Ok(None) | Ok(Some(false)) => (),
                    Ok(Some(true)) => break,
                }

                let _ = sqlx::query!("DELETE FROM prismarine_rusted.invites WHERE deletion_time IS NOT NULL AND deletion_time < ?", chrono::offset::Utc::now().timestamp())
                    .execute(&mut conn).await.map_err(|e| error!("Failure in database execution {:?}", e));

                tokio::time::delay_for(chrono::Duration::seconds(60).to_std().unwrap()).await;
            }
        })
    };
    let client_task = tokio::task::spawn(async move {
        let res = client.start().await;
        transmitter.broadcast(true).unwrap();
        res
    });

    let (client_res, _, _, _) =
        tokio::join!(client_task, dbl_task, team_clean_task, invite_clean_task);

    if let Err(e) = client_res {
        error!("Unexpected error while closing client: {:?}", e);
    }
}
