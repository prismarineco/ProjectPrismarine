#![allow(clippy::non_ascii_literal)]
use crate::utils::constants::emoji;
use crate::ShardManagerContainer;
use chrono::offset::Utc;
use dbl::widget::LargeWidget;
use log::*;
use serenity::utils::Colour as Color;
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::prelude::*,
    prelude::*,
};
use std::ops::Deref;
use tokio::stream::StreamExt;

#[command]
#[owners_only]
pub async fn logout(ctx: &Context, msg: &Message) -> CommandResult {
    let dat = ctx.data.read().await;

    let ask = msg
        .channel_id
        .say(ctx, "Are you sure you want to shut down the bot?")
        .await
        .unwrap();

    ask.react(
        ctx,
        channel::ReactionType::Unicode(emoji::CROSS_MARK.into()),
    )
    .await
    .unwrap();
    ask.react(
        ctx,
        channel::ReactionType::Unicode(emoji::CHECK_MARK.into()),
    )
    .await
    .unwrap();

    let react = serenity::collector::ReactionCollectorBuilder::new(ctx)
        .filter(|r| match &r.emoji {
            serenity::model::channel::ReactionType::Custom { .. } => false,
            serenity::model::channel::ReactionType::Unicode(em) => {
                em.as_str() == emoji::CROSS_MARK || em.as_str() == emoji::CHECK_MARK
            }
            _ => unreachable!(),
        })
        .author_id(msg.author.id)
        .message_id(ask.id)
        .channel_id(ask.channel_id)
        .timeout(std::time::Duration::from_secs(60))
        .collect_limit(1)
        .removed(false);
    let react = if let Some(id) = ask.guild_id {
        react.guild_id(id)
    } else {
        react
    };

    let mut react = react.await;

    if let Some(resp) = react.next().await {
        if let serenity::collector::reaction_collector::ReactionAction::Added(resp) = resp.deref() {
            if let serenity::model::channel::ReactionType::Unicode(resp) = &resp.deref().emoji {
                #[allow(clippy::non_ascii_literal)]
                {
                    if resp.as_str() == emoji::CHECK_MARK {
                        if let Some(manager) = dat.get::<ShardManagerContainer>() {
                            msg.channel_id.delete_message(ctx, ask.id).await.unwrap();
                            let _ = msg
                                .channel_id
                                .say(
                                    ctx,
                                    format!(
                                        "*Shutting down {}...*",
                                        ctx.http.get_current_user().await.unwrap().tag()
                                    ),
                                )
                                .await;
                            manager.lock().await.shutdown_all().await;
                        } else {
                            let _ = msg.reply(ctx, "Command Failed - Could not retrieve shard manager, bot will need to be shutdown manually.").await;
                            error!("Was unable to retrieve shard manager at command invocation");
                        }
                    } else if resp.as_str() == emoji::CROSS_MARK {
                        msg.channel_id
                            .say(ctx, "Understood. Aborting shutdown.")
                            .await
                            .unwrap();
                        msg.channel_id.delete_message(ctx, ask.id).await.unwrap();
                    }
                }
            }
        }
    } else {
        msg.channel_id
            .say(ctx, "Command Timed Out - Shutdown cancelled.")
            .await
            .unwrap();
        msg.channel_id.delete_message(ctx, ask.id).await.unwrap();
    }
    Ok(())
}

#[command]
#[owners_only]
pub async fn latency(ctx: &Context, msg: &Message) -> CommandResult {
    let dat = ctx.data.read().await;

    let shard_manager = if let Some(v) = dat.get::<ShardManagerContainer>() {
        v
    } else {
        let _ = msg
            .reply(ctx, "Command Failed - Could not retrieve shard manager")
            .await;
        error!("Failed to retrieve shard manager at latency command invoke.");
        return Ok(());
    };
    let manager = shard_manager.lock().await;
    let runners = manager.runners.lock().await;
    let mut res: Vec<String> = Vec::new();
    for runner in (*runners).values() {
        res.push(format!("`{:#?}`", runner.latency.unwrap_or_default()));
    }
    let name = ctx.http.get_current_user().await.unwrap().name;
    msg.channel_id
        .send_message(ctx, |m| {
            m.embed(|e| {
                e.title(format!("{} - Latency Report", name));
                for (index, string) in res.iter().enumerate() {
                    e.field(
                        format!("Shard #{}'s Latency:", index),
                        string.to_string(),
                        true,
                    );
                }
                e.color(Color::from_rgb(255, 0, 0));
                e
            });
            m
        })
        .await?;
    Ok(())
}

#[command]
#[owners_only]
pub async fn user(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let user: Option<User> = if args.is_empty() {
        match ctx.http.get_user(*msg.author.id.as_u64()).await {
            Ok(v) => Some(v),
            Err(e) => {
                error!("Could not retrieve user data: {:#?}", e);
                let _ = msg
                    .reply(ctx, "Command Failed - Could not retrieve user data.")
                    .await;
                return Ok(());
            }
        }
    } else {
        match args.parse::<u64>() {
            Ok(id) => {
                if let Ok(v) = ctx.http.get_user(id).await {
                    args.advance();
                    Some(v)
                } else {
                    let _ = msg
                        .reply(ctx, "Command Failed - Invalid User ID passed.")
                        .await;
                    return Ok(());
                }
            }
            Err(_) => {
                if msg.mentions.is_empty() {
                    let _ = msg.reply(ctx, "Command Failed - User not specified.").await;
                    return Ok(());
                } else {
                    Some(
                        match ctx.http.get_user(*msg.mentions[0].id.as_u64()).await {
                            Ok(v) => v,
                            Err(e) => {
                                error!("Could not retrieve user data: {:#?}", e);
                                let _ = msg
                                    .reply(ctx, "Command Failed - Could not retrieve user data.")
                                    .await;
                                return Ok(());
                            }
                        },
                    )
                }
            }
        }
    };

    let user: User = user.unwrap();

    let _ = msg
        .channel_id
        .send_message(ctx, |m| {
            m.embed(|e| {
                e.title(format!(
                    "User Report - {}#{}",
                    user.name, user.discriminator
                ));
                e.field("Discord ID:", format!("`{}`", user.id.as_u64()), true);
                e.field(
                    "Creation Date:",
                    format!("`{}`", user.created_at().to_rfc2822()),
                    true,
                );
                e.field(
                    "Account Type:",
                    if user.bot { "`Bot`" } else { "`Standard`" },
                    true,
                );
                e.thumbnail(user.face());
                e.footer(|f| f.text(format!("Report Generated at {}", Utc::now().to_rfc2822())))
            })
        })
        .await;

    Ok(())
}

#[command]
pub async fn info(ctx: &Context, msg: &Message) -> CommandResult {
    let widget = LargeWidget::new()
        .top_color("b30000")
        .build(*ctx.http.get_current_user().await.unwrap().id.as_u64())
        .unwrap()
        .into_string()
        .replace(".svg", ".png");
    let _ = msg.channel_id.send_message(ctx, |m| {
        m.embed(|e| {
            e.image(widget);
            e
        });
        m
    });

    Ok(())
}

#[command]
pub async fn print_reactions(ctx: &Context, msg: &Message) -> CommandResult {
    let target = msg.channel_id.say(ctx, "react here").await.unwrap();

    let mut listener = serenity::collector::ReactionCollectorBuilder::new(ctx)
        .message_id(target.id)
        .timeout(std::time::Duration::from_secs(60))
        .await;

    while let Some(r) = listener.next().await {
        println!("{:?}", r);
    }
    Ok(())
}
