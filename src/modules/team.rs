#![allow(warnings)]
use crate::get_player;
use crate::get_pool_from_ctx;
use crate::utils::constants::{emoji, values};
use crate::utils::db::{Invite as ProfileInvite, Player, Team, Tournament};
use crate::utils::misc::{ModelError, NFKind};
use chrono::prelude::*;
use image::png::PNGEncoder;
use image::ColorType;
use serenity::{
    builder::CreateEmbed,
    framework::standard::{macros::command, Args, CommandResult},
    http::AttachmentType,
    model::prelude::*,
    prelude::*,
    utils::Color,
};
use std::convert::{TryFrom, TryInto};
use std::ops::Deref;
use std::time::Duration;
use tokio::stream::StreamExt;

#[command("new")]
#[aliases("init")]
#[usage("<TEAM_NAME>")]
#[example("The Default Team")]
#[description("Register a team with the bot. You must already have registered a player profile in order to register a team.")]
pub async fn team_new(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let mut new_cap = get_player!(ctx, msg, *msg.author.id.as_u64());
    let name: String = if args.is_empty() {
        msg.channel_id
            .say(ctx, "Command Failed - No name specified.")
            .await
            .unwrap();
        return Ok(());
    } else {
        args.rest().into()
    };
    if let Err(e) = Team::add_to_db(
        &mut get_pool_from_ctx!(ctx).acquire().await.unwrap(),
        &mut new_cap,
        &name,
    )
    .await
    {
        msg.reply(
            ctx,
            "Command failed - An error occurred! Contact the developers immediately!",
        )
        .await
        .unwrap();
        error!(
            "Failed to add new team for {:?}\n\nError: {:#?}",
            new_cap, e
        );
    } else {
        msg.channel_id
            .say(ctx, "Your new team has been registered. Godspeed.")
            .await
            .unwrap();
    }
    Ok(())
}

lazy_static! {
    static ref REACT_LITERAL_VEC: Vec<&'static str> =
        vec![emoji::ONE, emoji::TWO, emoji::THREE, emoji::FOUR];
}

#[command("update")]
#[aliases("up", "set")]
#[description("Update your team's profile.")]
pub async fn team_update(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    let mut team = match Team::from_db(
        &mut get_pool_from_ctx!(ctx).acquire().await.unwrap(),
        *msg.author.id.as_u64(),
    )
    .await
    {
        Err(ModelError::NotFound(NFKind::Team(_), _)) => {
            msg.channel_id
                .say(
                    ctx,
                    "Command Failed - You don't have a team to use this command with!",
                )
                .await
                .unwrap();
            return Ok(());
        }
        Err(e) => {
            msg.channel_id.say(ctx, "Command Failed - An internal error has occurred! Contact the developers immediately!").await.unwrap();
            error!("Failed to get team for modification\n\nError: {:?}", e);
            return Ok(());
        }
        Ok(v) => v,
    };

    let mut ask = msg.channel_id.send_message(ctx, |m| {
        m.embed(|e| {
            e
                .color(Color::from_rgb(255, 0, 0))
                .description("What part of your team's profile would you like to update?")
                .fields(vec![
                    ("Name :one:", "Set your team's name to something fresh and new!", true),
                    ("Description :two:", "Set your team's description. Tell your story (in less than 2048 characters)!", true),
                    ("Thumbnail :three:", "Set your team's thumbnail. Add a bit of *pizazz* to your profile!", true),
                    ("Recruiting :four:", "Set your recruiting status. Tell the world you're welcomin' new players! (Accepted input: 'yes'/'no')", true)
                ])
        })
    }).await.unwrap();
    let mut react = serenity::collector::ReactionCollectorBuilder::new(ctx)
        .filter(|r| {
            if let channel::ReactionType::Unicode(em) = &r.deref().emoji {
                REACT_LITERAL_VEC.contains(&em.as_str())
            } else {
                false
            }
        })
        .author_id(msg.author.id)
        .message_id(ask.id)
        .channel_id(ask.channel_id)
        .timeout(Duration::from_secs(60))
        .removed(false)
        .await;

    for emoji in REACT_LITERAL_VEC.iter() {
        ask.react(ctx, channel::ReactionType::Unicode(emoji.to_string()))
            .await
            .unwrap();
    }

    if let Some(resp) = react.next().await {
        let mut modified = false;
        ask.delete_reactions(ctx).await.unwrap();
        if let serenity::collector::reaction_collector::ReactionAction::Added(resp) = resp.deref() {
            if let serenity::model::channel::ReactionType::Unicode(resp) = &resp.deref().emoji {
                match resp.as_str() {
                    "1\u{fe0f}\u{20e3}" => {
                        ask.edit(ctx, |m| {
                            m.suppress_embeds(true)
                                .content("What would you like to set you name to?")
                        })
                        .await
                        .unwrap();
                        if let Some(new_name) = msg
                            .author
                            .await_reply(ctx)
                            .timeout(Duration::from_secs(60))
                            .channel_id(msg.channel_id)
                            .await
                        {
                            team.set_name(new_name.content.clone());
                            modified = true;
                            new_name.delete(ctx).await.unwrap();
                        }
                    }
                    "2\u{fe0f}\u{20e3}" => {
                        ask.edit(ctx, |m| {
                            m.suppress_embeds(true)
                                .content("What will be your new description?")
                        })
                        .await
                        .unwrap();
                        let new_desc = msg
                            .author
                            .await_reply(ctx)
                            .timeout(Duration::from_secs(60))
                            .channel_id(msg.channel_id)
                            .await;
                        let new_desc = if let Some(msg) = new_desc {
                            msg.delete(ctx).await.unwrap();
                            Some(msg.content.clone())
                        } else {
                            None
                        };
                        if team.mod_desc(new_desc).is_err() {
                            msg.channel_id
                                .say(ctx, "Command Failed - Invalid description passed.")
                                .await
                                .unwrap();
                            return Ok(());
                        } else {
                            modified = true;
                        }
                    }
                    "3\u{fe0f}\u{20e3}" => {
                        ask.edit(ctx, |m| {
                            m.suppress_embeds(true)
                                .content("What will be your new thumbnail?")
                        })
                        .await
                        .unwrap();
                        let new_thumbnail = msg
                            .author
                            .await_reply(ctx)
                            .timeout(Duration::from_secs(60))
                            .channel_id(msg.channel_id)
                            .await;
                        let new_thumbnail = if let Some(msg) = new_thumbnail {
                            msg.delete(ctx).await.unwrap();
                            if msg.content.is_empty() {
                                msg.attachments.get(0).map(|x| x.proxy_url.clone())
                            } else {
                                Some(msg.content.clone())
                            }
                        } else {
                            None
                        };
                        team.set_thumbnail(new_thumbnail);
                        modified = true;
                    }
                    "4\u{fe0f}\u{20e3}" => {
                        ask.edit(ctx, |m| {
                            m.suppress_embeds(true)
                                .content("So, are you looking for new members or not? [yes/no]")
                        })
                        .await
                        .unwrap();
                        if let Some(new_status) = msg
                            .author
                            .await_reply(ctx)
                            .timeout(Duration::from_secs(60))
                            .channel_id(msg.channel_id)
                            .filter(|input| {
                                input.content.to_lowercase() == "yes"
                                    || input.content.to_lowercase() == "no"
                            })
                            .await
                        {
                            match new_status.content.as_str() {
                                "yes" => team.recruiting = true,
                                "no" => team.recruiting = false,
                                _ => (),
                            }
                            modified = true;
                            new_status.delete(ctx).await.unwrap();
                        }
                    }
                    _ => (),
                }
            }
        }
        if modified {
            if let Err(e) = team
                .update(&mut get_pool_from_ctx!(ctx).acquire().await.unwrap())
                .await
            {
                msg.channel_id
                    .say(ctx, "Command Failed - An internal error occurred!")
                    .await
                    .unwrap();
                error!(
                    "Error while updating data for Team: {:?}\n\nError: {:?}",
                    team, e
                );
            } else {
                msg.channel_id
                    .say(ctx, "Your team has been updated.")
                    .await
                    .unwrap();
            }
        }
        ask.delete(ctx).await.unwrap();
    }

    Ok(())
}

#[command("delete")]
#[aliases("del", "rm")]
#[description("Delete your team.")]
pub async fn team_delete(ctx: &Context, msg: &Message) -> CommandResult {
    let mut team = match Team::from_db(
        &mut get_pool_from_ctx!(ctx).acquire().await.unwrap(),
        *msg.author.id.as_u64(),
    )
    .await
    {
        Err(ModelError::NotFound(NFKind::Team(_), _)) => {
            msg.channel_id
                .say(ctx, "Command Failed - You do not have a team.")
                .await
                .unwrap();
            return Ok(());
        }
        Err(e) => {
            msg.channel_id.say(ctx, "Command Failed - An internal error has occurred! Contact the developers immediately!").await.unwrap();
            error!("Failed to get team for modification\n\nError: {:?}", e);
            return Ok(());
        }
        Ok(v) => v,
    };

    if team.del_time().is_some() {
        let ask = msg
            .channel_id
            .say(
                ctx,
                "Your team is already in queue for deletion. Do you want to recover it?",
            )
            .await
            .unwrap();
        ask.react(
            ctx,
            channel::ReactionType::Unicode(emoji::REGIONAL_INDICATOR_N.into()),
        )
        .await
        .unwrap();
        ask.react(
            ctx,
            channel::ReactionType::Unicode(emoji::REGIONAL_INDICATOR_Y.into()),
        )
        .await
        .unwrap();
        let resp = msg
            .author
            .await_reaction(ctx)
            .timeout(Duration::from_secs(60))
            .message_id(ask.id)
            .filter(|r| {
                if let serenity::model::channel::ReactionType::Unicode(em) = &r.emoji {
                    em.as_str() == emoji::REGIONAL_INDICATOR_N
                        || em.as_str() == emoji::REGIONAL_INDICATOR_Y
                } else {
                    false
                }
            })
            .await;

        if let Some(resp) = resp {
            // /*
            if let serenity::collector::reaction_collector::ReactionAction::Added(resp) =
                resp.deref()
            {
                if let serenity::model::channel::ReactionType::Unicode(resp) = &resp.deref().emoji {
                    match resp.as_str() {
                        em if em == emoji::REGIONAL_INDICATOR_Y => {
                            team.set_del_time(false, None);
                            if let Err(e) = team
                                .update(&mut get_pool_from_ctx!(ctx).acquire().await.unwrap())
                                .await
                            {
                                error!(
                                    "Failed to set deletion time for Team {:?}\n\nError: {:?}",
                                    team, e
                                );
                                msg.channel_id.say(ctx, "Command Failed - An internal error occurred! Contact the developers immediately!").await.unwrap();
                                return Ok(());
                            }
                            ask.delete(ctx).await.unwrap();
                            msg.channel_id
                                .say(ctx, "Alright. Your team has been recovered.")
                                .await
                                .unwrap();
                        }
                        em if em == emoji::REGIONAL_INDICATOR_N => {
                            ask.delete(ctx).await.unwrap();
                        }
                        _ => (),
                    }
                }
            }
        }
    } else {
        let ask = msg
            .channel_id
            .say(
                ctx,
                "This is a destructive operation. Are you sure you wish to delete your team?",
            )
            .await
            .unwrap();
        ask.react(
            ctx,
            channel::ReactionType::Unicode(emoji::REGIONAL_INDICATOR_N.into()),
        )
        .await
        .unwrap();
        ask.react(
            ctx,
            channel::ReactionType::Unicode(emoji::REGIONAL_INDICATOR_Y.into()),
        )
        .await
        .unwrap();
        let resp = msg
            .author
            .await_reaction(ctx)
            .timeout(Duration::from_secs(60))
            .message_id(ask.id)
            .filter(|r| {
                if let serenity::model::channel::ReactionType::Unicode(em) = &r.emoji {
                    em.as_str() == emoji::REGIONAL_INDICATOR_N
                        || em.as_str() == emoji::REGIONAL_INDICATOR_Y
                } else {
                    false
                }
            })
            .await;

        if let Some(resp) = resp {
            if let serenity::collector::reaction_collector::ReactionAction::Added(resp) =
                resp.deref()
            {
                if let serenity::model::channel::ReactionType::Unicode(resp) = &resp.deref().emoji {
                    match resp.as_str() {
                        em if em == emoji::REGIONAL_INDICATOR_Y => {
                            team.set_del_time(true, Some(values::DEL_BUFFER_DAYS));
                            if let Err(e) = team
                                .update(&mut get_pool_from_ctx!(ctx).acquire().await.unwrap())
                                .await
                            {
                                error!(
                                    "Failed to set deletion time for Team {:?}\n\nError: {:?}",
                                    team, e
                                );
                                msg.channel_id.say(ctx, "Command Failed - An internal error occurred! Contact the developers immediately!").await.unwrap();
                                return Ok(());
                            }
                            ask.delete(ctx).await.unwrap();
                            msg.channel_id.say(
                                ctx,
                                format!(
                                    "Alright. Your team will be deleted within {} days. After that, you will be able to make a new team.",
                                    values::DEL_BUFFER_DAYS)
                                )
                                .await
                                .unwrap();
                        }
                        em if em == emoji::REGIONAL_INDICATOR_N => {
                            ask.delete(ctx).await.unwrap();
                        }
                        _ => (),
                    }
                }
            }
        }
    }

    Ok(())
}

#[command("invite_player")]
#[aliases("invite", "add")]
#[usage("<PLAYER_ID or PLAYER_MENTION>")]
#[example("@Alex Bluefall#3305")]
#[example("196470965402730507")]
#[description("Invite a player to your team! This only works if the recieving player is marked as a free agent and is not part of a team, and if the team using it is marked as recruiting.")]
pub async fn team_invite(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    // STEP 1: Get both the active team and requested player from the database.
    let c = &mut get_pool_from_ctx!(ctx).acquire().await.unwrap();
    let team: Team = match Team::from_db(&mut *c, *msg.author.id.as_u64()).await {
        Err(ModelError::NotFound(NFKind::Team(_), _)) => {
            msg.channel_id
                .say(ctx, "Command Failed - You do not have a team.")
                .await
                .unwrap();
            return Ok(());
        }
        Err(e) => {
            msg.channel_id.say(ctx, "Command Failed - An internal error has occurred! Contact the developers immediately!").await.unwrap();
            error!("Failed to get team for modification\n\nError: {:?}", e);
            return Ok(());
        }
        Ok(v) => v,
    };

    let player_id = if let Some(mention) = msg.mentions.get(0) {
        *mention.id.as_u64()
    } else if let Ok(id) = args.single::<u64>() {
        id
    } else {
        msg.channel_id
            .say(ctx, "Command Failed - Invalid player identifier provided.")
            .await
            .unwrap();
        return Ok(());
    };

    let player: Player = match Player::from_db(&mut *c, player_id).await {
        Ok(v) => v,
        Err(ModelError::NotFound(NFKind::Player(id), _)) => {
            msg.channel_id
                .say(
                    ctx,
                    format!("Command Failed - No player with ID `{}` found.", id),
                )
                .await
                .unwrap();
            return Ok(());
        }
        Err(e) => {
            error!(
                "Failed to retrieve player #{} while dispatching invite\n\nError: {:?}",
                player_id, e
            );
            msg.channel_id.say(ctx, "Command Failed - An internal error occurred! Contact the developers immediately!").await.unwrap();
            return Ok(());
        }
    };

    // STEP 2: Verify that a player won't overwrite their current team by recieving an invite, or a
    // team that will invite the player is configured to accept them.
    if !player.is_free_agent() || player.team_id().is_some() {
        msg.channel_id.say(ctx, "Command Failed - Player is not an available free agent. To qualify as a free agent, they must not have a set team, and have their profile set as an FA.").await.unwrap();
        return Ok(());
    } else if !team.recruiting {
        msg.channel_id.say(ctx, "Command Failed - Your team is not currently set to accept new players. To allow your team to invite new players, set your recruiting status via `team update`.").await.unwrap();
        return Ok(());
    }

    // STEP 3: Now that all predicates are satisfied, we can ask the captain to provide a message
    // to send (if they want), and send off their invite!
    msg.channel_id.say(ctx, "Is there a message you would like to include with your invite? [Reply with `NO MESSAGE` to not include a message]").await.unwrap();
    let resp = msg
        .author
        .await_reply(ctx)
        .timeout(Duration::from_secs(300))
        .channel_id(msg.channel_id)
        .await;

    let text = if let Some(text) = resp {
        if text.content.to_uppercase() == "NO MESSAGE".to_owned() {
            None
        } else {
            Some(text.content.clone())
        }
    } else {
        None
    };

    match ctx.http.get_user(*player.id()).await {
        Err(e) => {
            error!("Failed to retrieve user #{}\n\nError: {:?}", player_id, e);
            msg.channel_id
                .say(
                    ctx,
                    "Command Failed - Could not retrieve user from the Discord API.",
                )
                .await
                .unwrap();
            return Ok(());
        }
        Ok(u) => {
             if let Err(serenity::Error::Http(e)) = u.direct_message(ctx, |m| {
                m.embed(|e| {
                    e
                        .title("You've recieved an invite!")
                        .description(
                            format!(
                                "A captain from another team has decided to welcome you into their roster!\n\nMESSAGE FROM THE CAPTAIN:\n{}",
                                text.clone().unwrap_or("N/A".into())
                                )
                            )
                        .field("Team Name:", team.name(), true)
                        .field("Team Captain:", msg.author.tag(), true)
                        .footer(|f| {
                            f.text("This invite will expire in 3 days, so make your choice before that happens.")
                        })
                })
            }).await {
                 if let serenity::http::error::Error::UnsuccessfulRequest(erresp) = &*e {
                     if erresp.status_code.as_u16() == 403 {
                         msg.channel_id.say(ctx, "Failed to notify recipient of invite. Your invite will still be dispatched, but you will need to notify them manually of it.").await.unwrap();
                     } else {
                         error!("Unknown error from the discord API: {:?}", e);
                         msg.channel_id.say(ctx, "Command Failed - An error occurred with the Discord API.").await.unwrap();
                         return Ok(());
                     }
                 } else {
                     error!("Unknown error from the discord API: {:?}", e);
                     msg.channel_id.say(ctx, "Command Failed - An error occurred with the Discord API.").await.unwrap();
                     return Ok(());
                 }
            }
        }
    }

    if let Err(e) = ProfileInvite::add_to_db(
        &mut *c,
        &player,
        &team,
        &text,
        Utc::now() + chrono::Duration::days(3),
    )
    .await
    {
        error!("Failed to register invite\n\nError: {:?}", e);
        msg.channel_id
            .say(
                ctx,
                "Command Failed - An internal error occurred! Contact the developers immediately!",
            )
            .await
            .unwrap();
    } else {
        msg.channel_id
            .say(
                ctx,
                "Your invite has been dispatched. It will expire within 3 days.",
            )
            .await
            .unwrap();
    }

    Ok(())
}

#[command("register_tournament")]
#[aliases("rt", "reg_tourney")]
#[usage("<PLACEMENT> <TOURNAMENT>")]
#[example("3 Low Ink")]
#[description(
    "Register a tournament result with your team. This will appear on your team's profile."
)]
pub async fn team_register_tournament(
    ctx: &Context,
    msg: &Message,
    mut args: Args,
) -> CommandResult {
    // FIRST: Get the user's team.
    let c = &mut get_pool_from_ctx!(ctx).acquire().await.unwrap();
    let mut team: Team = match Team::from_db(&mut *c, *msg.author.id.as_u64()).await {
        Err(ModelError::NotFound(NFKind::Team(_), _)) => {
            msg.channel_id
                .say(ctx, "Command Failed - You do not have a team.")
                .await
                .unwrap();
            return Ok(());
        }
        Err(e) => {
            msg.channel_id.say(ctx, "Command Failed - An internal error has occurred! Contact the developers immediately!").await.unwrap();
            error!("Failed to get team for modification\n\nError: {:?}", e);
            return Ok(());
        }
        Ok(v) => v,
    };

    // NEXT: Get the placement of the team.
    let place: i16 = match args.single() {
        Ok(v) => v,
        Err(e) => {
            msg.channel_id
                .say(ctx, "Command Failed - Invalid parameter passed.")
                .await
                .unwrap();
            return Ok(());
        }
    };

    // THEN: Get the tournament's name.
    let name = args.rest();

    // FINALLY: register the result.
    team.add_tournament(Tournament::new(name.into(), place, Utc::now()));
    if let Err(e) = team.update(c).await {
        error!(
            "Failed to register tournament result for team {}\n\nError: {:?}",
            team.id(),
            e
        );
        msg.channel_id.say(ctx, "Command Failed - An internal error occurred! Contact the developers of the bot immediately!").await.unwrap();
    } else {
        msg.channel_id.say(ctx, "Result registered.").await.unwrap();
    }

    Ok(())
}

#[command("show")]
#[aliases("profile", "get")]
#[usage("[CAPTAIN_ID or CAPTAIN_MENTION]")]
#[description("Show a team's profile. If invoked with an @ mention or ID, the bot will attempt to retrive the profile associated with that ID. Otherwise it will retrive the team of the command's user.")]
pub async fn team_show(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    // STEP 1: We get the team in question's ID (we also check if the user wants their own team, so
    // we can display a more helpful error message in that case).
    let mut self_retrieve = false;
    let team_id: u64 = if args.is_empty() {
        self_retrieve = true;
        *msg.author.id.as_u64()
    } else if msg.mentions.len() == 1 {
        *msg.mentions[0].id.as_u64()
    } else if let Ok(v) = args.single::<u64>() {
        v
    } else {
        msg.channel_id
            .say(ctx, "Command Failed - Invalid argument passed.")
            .await
            .unwrap();
        return Ok(());
    };

    // Now we retrieve the user's team!
    let mut c = get_pool_from_ctx!(ctx).acquire().await.unwrap();
    let team: Team = match Team::from_db(&mut c, team_id).await {
        Ok(v) => v,
        Err(ModelError::NotFound(NFKind::Team(_), _)) => {
            let notif = if self_retrieve {
                "Command Failed - You don't have a team! Set one up with `team new`, and invite players with `team invite_player`.".to_owned()
            } else {
                format!(
                    "Command Failed - Team ID {} is not in the database.",
                    team_id
                )
            };
            msg.channel_id.say(ctx, notif).await.unwrap();
            return Ok(());
        }
        Err(e) => {
            error!(
                "Failed to retrieve Team with ID {}\n\nError: {:?}",
                team_id, e
            );
            msg.channel_id.say(ctx, "Command Failed - An internal error occurred! Contact the developers immediately!").await.unwrap();
            return Ok(());
        }
    };
    trace!("Retrieved team for display with ID: {}", team.id());

    msg.channel_id.broadcast_typing(ctx).await.unwrap();

    // This provides us a channel to *very* temporarily upload people's loadouts to. That way, we
    // can take advantage of Discord's CDN and put the attachment urls *in* the data.
    let attach_channel: GuildChannel = match ctx
        .http
        .get_channel(
            *ctx.data
                .read()
                .await
                .get::<crate::AttachChannelID>()
                .unwrap(),
        )
        .await
    {
        Ok(v) => v.guild().unwrap(),
        Err(e) => {
            error!("Failed to get attachment channel: {:?}", e);
            msg.channel_id.say(ctx, "Command Failed - An internal error has occurred! Contact the developers immediately!").await.unwrap();
            return Ok(());
        }
    };
    trace!("Attachment channel obtained");

    let mut player_embeds: Vec<(usize, CreateEmbed)> = Vec::new();

    for (idx, player) in team.players().iter().enumerate() {
        let i = player.loadout().to_img().unwrap().to_rgba();
        let w = i.width();
        let h = i.height();

        let mut buf: Vec<u8> = Vec::new();

        let encoder = PNGEncoder::new(&mut buf);
        encoder
            .encode(&i.into_raw(), w, h, ColorType::RGBA(8))
            .unwrap();

        let attachment_url = {
            // Here, we fire off an image of the player's loadout. This is so we can take advantage
            // of Discord's CDN and display people's loadouts within the team profile, while still
            // allowing for pagination. We *miiiiiiiiiiiiiiiiiiight* run into rate-limiting issues
            // in the future, though.
            let mut msg = attach_channel
                .send_files(
                    ctx,
                    vec![AttachmentType::Bytes {
                        data: std::borrow::Cow::from(buf),
                        filename: "ld.png".into(),
                    }]
                    .into_iter(),
                    |m| m,
                )
                .await
                .unwrap();

            // Now we just swipe the attachment url
            let v = msg.attachments[0].proxy_url.clone();
            // And now return the url for use in the embed!
            v
        };

        let usr = match ctx.http.get_user(*player.id()).await {
            Ok(v) => v,
            Err(e) => {
                error!(
                    "Failed to retrieve user with ID #{} from Discord API\n\nError: {:?}",
                    player.id(),
                    e
                );
                msg.channel_id
                    .say(
                        ctx,
                        format!("Command Failed - Discord User `{}` not found.", player.id()),
                    )
                    .await
                    .unwrap();
                return Ok(());
            }
        };

        // Code nicked from the player profiler below. Waste not want not :P
        let mut em = CreateEmbed::default();
        em.title(format!("Player #{} - {}", idx + 1, usr.name))
            .field("IGN:", player.name(), true)
            .field(
                "Friend Code",
                if player.is_private {
                    if *msg.author.id.as_u64() == *player.id() {
                        player.fc()
                    } else {
                        "SW-////-////-////"
                    }
                } else {
                    player.fc()
                },
                true,
            )
            .field("Level:", player.level, true)
            .field("Position:", player.position, true)
            .thumbnail(usr.face())
            .color(Color::from_rgb(255, 0, 0))
            .image(attachment_url);

        for (mode, rank) in &player.ranks() {
            em.field(mode, rank, true);
        }
        trace!(
            "Generated embed for player {} in team {}: {:#?}",
            player.id(),
            team.id(),
            em
        );
        player_embeds.push((idx, em));
    }

    let mut tourney_embed = CreateEmbed::default();

    if !team.tournaments().is_empty() {
        tourney_embed.title("Recent Tournament Results");
        for tourney in team.tournaments().iter().take(5) {
            tourney_embed
                .field(
                    format!("{}", tourney.name()),
                    format!(
                        "Place: {}\n\
                Result Registered: {}\n",
                        tourney.place(),
                        tourney.time().format("%F / %H:%M:%S %Z")
                    ),
                    true,
                )
                .color(Color::from_rgb(255, 0, 0));
        }
    } else {
        tourney_embed.description("No tournaments registered.");
    }

    let team_msg: Message = msg
        .channel_id
        .send_message(ctx, |m| {
            m.embed(|e| {
                e.title(format!("Team Profile - {}", team.name()))
                    .field("Team Captain", team.captain().name(), true)
                    .field("Team ID:", team.id(), true)
                    .field(
                        "Created At:",
                        team.time_created().format("%F / %H:%M:%S %Z"),
                        true,
                    );
                if let Some(v) = team.desc() {
                    e.description(v);
                }
                if let Some(v) = team.thumbnail() {
                    e.thumbnail(v);
                }
                e.color(Color::from_rgb(255, 0, 0));
                e
            })
        })
        .await
        .unwrap();
    let tourney_msg: Message = msg
        .channel_id
        .send_message(ctx, |m| {
            m.embed(|e| {
                *e = tourney_embed;
                e
            })
        })
        .await
        .unwrap();

    let mut player_msg: Message = msg
        .channel_id
        .send_message(ctx, |m| {
            // we can assume that the player vec will always have at least one member (i.e. the captain
            // themselves)
            m.embed(|e| {
                *e = player_embeds[0].1.clone();
                e
            })
        })
        .await
        .unwrap();
    let mut current_idx: usize = 0;
    player_msg
        .react(ctx, channel::ReactionType::Unicode(emoji::STRT_ITEM.into()))
        .await
        .unwrap();
    player_msg
        .react(ctx, channel::ReactionType::Unicode(emoji::PREV_ITEM.into()))
        .await
        .unwrap();
    player_msg
        .react(ctx, channel::ReactionType::Unicode(emoji::NEXT_ITEM.into()))
        .await
        .unwrap();
    player_msg
        .react(ctx, channel::ReactionType::Unicode(emoji::LAST_ITEM.into()))
        .await
        .unwrap();
    player_msg
        .react(ctx, channel::ReactionType::Unicode(emoji::STOP.into()))
        .await
        .unwrap();

    while let Some(input) = player_msg
        .await_reaction(ctx)
        .author_id(msg.author.id)
        .filter(|r| {
            if let serenity::model::channel::ReactionType::Unicode(em) = &r.emoji {
                em == emoji::STRT_ITEM
                    || em == emoji::PREV_ITEM
                    || em == emoji::NEXT_ITEM
                    || em == emoji::LAST_ITEM
                    || em == emoji::STOP
            } else {
                false
            }
        })
        .timeout(Duration::from_secs(60))
        .await
    {
        if let serenity::collector::ReactionAction::Added(input) = input.deref() {
            if let serenity::model::channel::ReactionType::Unicode(em) = &input.deref().emoji {
                match em {
                    em if em == emoji::STRT_ITEM => {
                        input.delete(ctx).await.unwrap();
                        current_idx = 0;
                        player_msg
                            .edit(ctx, |m| {
                                m.embed(|e| {
                                    *e = player_embeds[0].1.clone();
                                    e
                                })
                            })
                            .await
                            .unwrap();
                    }
                    em if em == emoji::LAST_ITEM => {
                        input.delete(ctx).await.unwrap();
                        current_idx = player_embeds.len() - 1;
                        player_msg
                            .edit(ctx, |m| {
                                m.embed(|e| {
                                    *e = player_embeds[current_idx].1.clone();
                                    e
                                })
                            })
                            .await
                            .unwrap();
                    }
                    em if em == emoji::NEXT_ITEM => {
                        input.delete(ctx).await.unwrap();
                        current_idx = if current_idx == player_embeds.len() - 1 {
                            0
                        } else {
                            current_idx + 1
                        };
                        player_msg
                            .edit(ctx, |m| {
                                m.embed(|e| {
                                    *e = player_embeds[current_idx].1.clone();
                                    e
                                })
                            })
                            .await
                            .unwrap();
                    }
                    em if em == emoji::PREV_ITEM => {
                        input.delete(ctx).await.unwrap();
                        current_idx = if current_idx == 0 {
                            player_embeds.len() - 1
                        } else {
                            current_idx - 1
                        };
                        player_msg
                            .edit(ctx, |m| {
                                m.embed(|e| {
                                    *e = player_embeds[current_idx].1.clone();
                                    e
                                })
                            })
                            .await
                            .unwrap();
                    }
                    em if em == emoji::STOP => {
                        team_msg.delete(ctx).await.unwrap();
                        tourney_msg.delete(ctx).await.unwrap();
                        player_msg.delete(ctx).await.unwrap();
                        return Ok(());
                    }
                    _ => (),
                }
            }
        }
    }

    Ok(())
}
