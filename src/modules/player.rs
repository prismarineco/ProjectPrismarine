#![allow(warnings)]
use crate::get_pool_from_ctx;
use crate::utils::constants::emoji;
use crate::utils::db::player::Position;
use crate::utils::db::Invite as TeamInvite;
use crate::utils::db::{Player, Team};
use crate::utils::misc::{ModelError, NFKind};
use image::png::PNGEncoder;
use image::ColorType;
use serenity::{
    builder::CreateEmbed,
    collector::{message_collector, reaction_collector},
    framework::standard::{macros::command, Args, CommandResult},
    http::AttachmentType,
    model::channel,
    model::prelude::*,
    prelude::*,
    utils::Colour as Color,
};
use std::convert::{TryFrom, TryInto};
use std::ops::Deref;
use std::time::Duration;

/// Macro to assist in retrieving players.
#[macro_export]
macro_rules! get_player {
    ($ctx:expr, $msg:expr, $id:expr) => {{
        let ctx: &Context = $ctx;
        let msg: &Message = $msg;
        let id: u64 = $id;
        let mut conn = get_pool_from_ctx!(ctx).acquire().await.unwrap();

        #[allow(unused_mut)]
        let mut plr = match Player::from_db(&mut conn, id).await {
            Ok(v) => v,
            Err(e) => if let ModelError::NotFound(kind, trace) = e.as_ref() {
                if let NFKind::Player(unfound_id) = kind {
                    let _ = msg.channel_id.say(ctx, &format!("Command Failed - Player ID `{}` not found in the database. Register a profile with `player new`.", unfound_id));
                } else {
                    let _ = msg.channel_id.say(ctx, "Command Failed - An error occured! Contact the developers immediately!");
                    error!(
                        "Something went sideways!\n\nError: {:?}\n\nBacktrace: {:#?}",
                        e, trace
                    );
                }
                return Ok(());
            } else {
                let _ = msg.channel_id.say(
                    ctx,
                    "Command Failed - An error occurred! Contact the developers immediately!",
                );
                error!("Something went sideways!\n\nError: {:#?}", e);
                return Ok(());
            }
        };
        plr
    }}
}

#[command("new")]
#[aliases("init", "create")]
#[description("Create a brand new profile. This does nothing if you already have a profile.")]
pub async fn player_new(ctx: &Context, msg: &Message) -> CommandResult {
    let mut conn = get_pool_from_ctx!(ctx).acquire().await.unwrap();

    match Player::add_to_db(&mut conn, *msg.author.id.as_u64()).await {
        Ok(_) => {
            let _ = msg
                .channel_id
                .say(ctx, "Added you to the database! ^^)")
                .await;
        }
        Err(e) => {
            error!("Failed to add player to database\n\nError: {:?}", e);
            let _ = msg.channel_id.say(ctx, "There was an issue with adding you to the database. Please contact the development team immediately.").await;
        }
    };

    Ok(())
}

#[command("update")]
#[aliases("up", "modify", "set")]
/// The profile customization command. This command has two modes of operation:
///
/// 1. GUI-based: Use reactions to navigate an embed-based menu. Good if you're new to the bot.
/// 2. Command-based: Use standard subcommands for configuration. This is good if you want a more
///    expedient method of configuration, or are familiar with usage of other bots. The subcommands
///    are indicated in [`brackets`] next to the option's name in the GUI. Further options are
///    generally mapped to their reaction equivalents (like `pr.player update private yes` to set
///    your privacy status, or `pr.player update ign foo` to set your in-game name to 'foo'.)
pub async fn player_update(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    // STEP 1: Get the specified player.
    let mut c = get_pool_from_ctx!(ctx).acquire().await.unwrap();
    let mut player = match Player::from_db(&mut c, *msg.author.id.as_u64()).await {
        Ok(v) => v,
        Err(ModelError::NotFound(NFKind::Player(_), _)) => {
            msg.channel_id
                .say(
                    ctx,
                    "Command Failed - You don't have a profile. Make one with `player new`.",
                )
                .await
                .unwrap();
            return Ok(());
        }
        Err(e) => {
            error!(
                "Failed to get player with ID #{}\n\nError: {:?}",
                msg.author.id.as_u64(),
                e
            );
            return Ok(());
        }
    };

    let mut modified = false;

    // We'll want to check if there are extra args here. If there are, delegate to an alternate
    // command. Otherwise, bring up the UI and allow the user to use that.
    if args.is_empty() {
        let mut query_msg =
            msg.channel_id
                .send_message(ctx, |m| {
                    m.embed(|e| {
                        e.description("How would you like to modify your profile?")
                            .color(Color::from_rgb(255, 0, 0))
                            .fields(vec![
                            (
                                ":one: Friend Code [`fc`]",
                                "Display your FC on your profile, so people can add you easily.",
                                true,
                            ),
                            (
                                ":two: In-Game Name [`ign`]",
                                "Make your name known with your profile.",
                                true,
                            ),
                            (
                                ":three: Level [`lv`]",
                                "Set your level and show off your experience.",
                                true,
                            ),
                            (
                                ":four: Ranks [`rk`]",
                                "Set your ranks, whether you're C+ or Top 500.",
                                true,
                            ), // This one's getting its own function. The nesting will be ridiculous.
                            (":five: Position [`pos`]", "Where do you go on a team?", true),
                            (":six: Loadout [`ld`]", "Show off your abilities with pride.", true),
                            (
                                ":seven: Free Agent Status [`fa`]",
                                "Set whether or not you're looking for a team.",
                                true,
                            ),
                            (
                                ":eight: Privacy Status [`pri`]",
                                "Hide your FC from eyes that aren't yours!",
                                true,
                            ),
                        ])
                    })
                })
                .await
                .unwrap();

        query_msg
            .react(ctx, channel::ReactionType::Unicode(emoji::ONE.into()))
            .await
            .unwrap();
        query_msg
            .react(ctx, channel::ReactionType::Unicode(emoji::TWO.into()))
            .await
            .unwrap();
        query_msg
            .react(ctx, channel::ReactionType::Unicode(emoji::THREE.into()))
            .await
            .unwrap();
        query_msg
            .react(ctx, channel::ReactionType::Unicode(emoji::FOUR.into()))
            .await
            .unwrap();
        query_msg
            .react(ctx, channel::ReactionType::Unicode(emoji::FIVE.into()))
            .await
            .unwrap();
        query_msg
            .react(ctx, channel::ReactionType::Unicode(emoji::SIX.into()))
            .await
            .unwrap();
        query_msg
            .react(ctx, channel::ReactionType::Unicode(emoji::SEVEN.into()))
            .await
            .unwrap();
        query_msg
            .react(ctx, channel::ReactionType::Unicode(emoji::EIGHT.into()))
            .await
            .unwrap();

        if let Some(input) = query_msg
            .await_reaction(ctx)
            .removed(false)
            .timeout(Duration::from_secs(60))
            .author_id(msg.author.id)
            .filter(|r| {
                if let channel::ReactionType::Unicode(em) = &r.emoji {
                    em == emoji::ONE
                        || em == emoji::TWO
                        || em == emoji::THREE
                        || em == emoji::FOUR
                        || em == emoji::FIVE
                        || em == emoji::SIX
                        || em == emoji::SEVEN
                        || em == emoji::EIGHT
                } else {
                    false
                }
            })
            .await
        {
            if let reaction_collector::ReactionAction::Added(em) = input.deref() {
                if let channel::ReactionType::Unicode(em) = &em.deref().emoji {
                    match em {
                        em if em == emoji::ONE => {
                            query_msg.delete_reactions(ctx).await.unwrap();
                            query_msg
                                .edit(ctx, |m| {
                                    m.embed(|e| {
                                        e.description("What's your FC? (Send `quit` to cancel)")
                                            .color(Color::from_rgb(255, 0, 0))
                                    })
                                })
                                .await
                                .unwrap();
                            loop {
                                if let Some(resp) = msg
                                    .author
                                    .await_reply(ctx)
                                    .channel_id(msg.channel_id)
                                    .timeout(Duration::from_secs(180))
                                    .await
                                {
                                    resp.delete(ctx).await.unwrap();
                                    if resp.content.to_ascii_lowercase().as_str() == "quit" {
                                        break;
                                    } else if player.set_fc(&resp.content).is_ok() {
                                        modified = true;
                                        break;
                                    } else {
                                        query_msg
                                            .edit(ctx, |m| {
                                                m.embed(|e| {
                                                    e.description("Invalid FC set. Try again.")
                                                })
                                            })
                                            .await
                                            .unwrap();
                                        continue;
                                    }
                                } else {
                                    break;
                                }
                            }
                        }
                        em if em == emoji::TWO => {
                            query_msg.delete_reactions(ctx).await.unwrap();
                            query_msg
                                .edit(ctx, |m| {
                                    m.embed(|e| {
                                        e.description(
                                            "What's your in-game name? (send `quit` to cancel)",
                                        )
                                        .color(Color::from_rgb(255, 0, 0))
                                    })
                                })
                                .await
                                .unwrap();

                            loop {
                                if let Some(resp) = msg
                                    .author
                                    .await_reply(ctx)
                                    .channel_id(msg.channel_id)
                                    .timeout(Duration::from_secs(180))
                                    .await
                                {
                                    resp.delete(ctx).await.unwrap();
                                    if player.set_name(resp.content.clone()).is_ok() {
                                        modified = true;
                                        break;
                                    } else if resp.content.to_ascii_lowercase().as_str() == "quit" {
                                        break;
                                    } else {
                                        query_msg
                                            .edit(ctx, |m| {
                                                m.embed(|e| {
                                                    e.description("Invalid name set. Try again.")
                                                })
                                            })
                                            .await
                                            .unwrap();
                                        continue;
                                    }
                                } else {
                                    break;
                                }
                            }
                        }
                        em if em == emoji::THREE => {
                            query_msg.delete_reactions(ctx).await.unwrap();
                            query_msg
                                .edit(ctx, |m| {
                                    m.embed(|e| {
                                        e.description("What's your level? (send `quit` to cancel)")
                                            .color(Color::from_rgb(255, 0, 0))
                                    })
                                })
                                .await
                                .unwrap();

                            loop {
                                if let Some(resp) = msg
                                    .author
                                    .await_reply(ctx)
                                    .channel_id(msg.channel_id)
                                    .timeout(Duration::from_secs(180))
                                    .await
                                {
                                    resp.delete(ctx).await.unwrap();

                                    if let Ok(v) = i32::from_str_radix(&resp.content, 10) {
                                        modified = true;
                                        player.level = v;
                                        break;
                                    } else if resp.content.to_ascii_lowercase().as_str() == "quit" {
                                        break;
                                    } else {
                                        query_msg
                                            .edit(ctx, |m| {
                                                m.embed(|e| {
                                                    e.description("Invalid level set. Try again.")
                                                })
                                            })
                                            .await
                                            .unwrap();
                                        continue;
                                    }
                                } else {
                                    break;
                                }
                            }
                        }
                        em if em == emoji::FOUR => {
                            // this is gonna be big, so it gets its own function.
                            modified = update_set_rank(ctx, msg, &mut query_msg, &mut player).await;
                        }
                        em if em == emoji::FIVE => {
                            query_msg.delete_reactions(ctx).await.unwrap();
                            query_msg
                                .edit(ctx, |m| {
                                    m.embed(|e| {
                                        e.description(format!(
                                            "What's your position?\n\
                                        {}: Not Set\n\
                                        {}: Frontline\n\
                                        {}: Midline\n\
                                        {}: Backline\n\
                                        {}: Flex\n\
                                        (send `quit` to cancel)",
                                            emoji::ONE,
                                            emoji::TWO,
                                            emoji::THREE,
                                            emoji::FOUR,
                                            emoji::FIVE,
                                        ))
                                        .color(Color::from_rgb(255, 0, 0))
                                    })
                                })
                                .await
                                .unwrap();

                            query_msg
                                .react(ctx, channel::ReactionType::Unicode(emoji::ONE.into()))
                                .await
                                .unwrap();
                            query_msg
                                .react(ctx, channel::ReactionType::Unicode(emoji::TWO.into()))
                                .await
                                .unwrap();
                            query_msg
                                .react(ctx, channel::ReactionType::Unicode(emoji::THREE.into()))
                                .await
                                .unwrap();
                            query_msg
                                .react(ctx, channel::ReactionType::Unicode(emoji::FOUR.into()))
                                .await
                                .unwrap();
                            query_msg
                                .react(ctx, channel::ReactionType::Unicode(emoji::FIVE.into()))
                                .await
                                .unwrap();

                            if let Some(input) = query_msg
                                .await_reaction(ctx)
                                .removed(false)
                                .timeout(Duration::from_secs(60))
                                .author_id(msg.author.id)
                                .filter(|r| {
                                    if let channel::ReactionType::Unicode(em) = &r.emoji {
                                        em == emoji::ONE
                                            || em == emoji::TWO
                                            || em == emoji::THREE
                                            || em == emoji::FOUR
                                            || em == emoji::FIVE
                                    } else {
                                        false
                                    }
                                })
                                .await
                            {
                                if let reaction_collector::ReactionAction::Added(em) = input.deref()
                                {
                                    if let channel::ReactionType::Unicode(em) = &em.deref().emoji {
                                        match em {
                                            em if em == emoji::ONE => {
                                                player.position = Position::NotSet;
                                                modified = true;
                                            }
                                            em if em == emoji::TWO => {
                                                player.position = Position::Frontline;
                                                modified = true;
                                            }
                                            em if em == emoji::THREE => {
                                                player.position = Position::Midline;
                                                modified = true;
                                            }
                                            em if em == emoji::FOUR => {
                                                player.position = Position::Backline;
                                                modified = true;
                                            }
                                            em if em == emoji::FIVE => {
                                                player.position = Position::Flex;
                                                modified = true;
                                            }
                                            _ => (),
                                        }
                                    }
                                }
                            }
                        }
                        em if em == emoji::SIX => {
                            query_msg.delete_reactions(ctx).await.unwrap();
                            query_msg
                                .edit(ctx, |m| {
                                    m.embed(|e| {
                                        e.description(
                                            "What's your loadout? (Send `quit` to cancel)",
                                        )
                                        .color(Color::from_rgb(255, 0, 0))
                                    })
                                })
                                .await
                                .unwrap();

                            loop {
                                if let Some(resp) = msg
                                    .author
                                    .await_reply(ctx)
                                    .channel_id(msg.channel_id)
                                    .timeout(Duration::from_secs(180))
                                    .await
                                {
                                    resp.delete(ctx).await.unwrap();
                                    if resp
                                        .content
                                        .starts_with("https://selicia.github.io/en_US/#")
                                    {
                                        let ldink_hex = resp.content.as_str().trim_start_matches(
                                            "https://selicia.github.io/en_US/#",
                                        );
                                        if ldink_hex.len() != 25 {
                                            query_msg
                                        .edit(ctx, |m| {
                                            m.embed(|e| e.description("Invalid loadout set. Make sure you're using `https://selicia.github.io/en_US/` for your loadout."))
                                        })
                                        .await
                                        .unwrap();
                                            continue;
                                        }
                                        match player.set_loadout(&mut c, ldink_hex).await {
                                            Ok(()) => modified = true,
                                            Err(ModelError::InvalidParameter(_))
                                            | Err(ModelError::NotFound(_, _)) => {
                                                query_msg
                                        .edit(ctx, |m| {
                                            m.embed(|e| e.description("Invalid loadout set. Make sure you're using `https://selicia.github.io/en_US/` for your loadout and try again."))
                                        })
                                        .await
                                        .unwrap();
                                                continue;
                                            }
                                            Err(e) => {
                                                error!("Failed to set loadout for player {}\n\nError: {:?}", player.id(), e);
                                                msg.channel_id.say(ctx, "Command Failed - An internal error has occurred! Contact the developers immediately!").await.unwrap();
                                                break;
                                            }
                                        }
                                        break;
                                    } else if resp.content.to_ascii_lowercase().as_str() == "quit" {
                                        break;
                                    } else {
                                        query_msg
                                        .edit(ctx, |m| {
                                            m.embed(|e| e.description("Invalid loadout set. Make sure you're using `https://selicia.github.io/en_US/` for your loadout and try again."))
                                        })
                                        .await
                                        .unwrap();
                                        continue;
                                    }
                                } else {
                                    break;
                                }
                            }
                        }
                        em if em == emoji::SEVEN => {
                            query_msg.delete_reactions(ctx).await.unwrap();
                            if *player.id() == player.team_id().unwrap_or(0) {
                                msg.channel_id.say(ctx, "Command Failed - You are currently a captain! In order to set yourself as a free agent, you need to delete your team with `team delete`.").await.unwrap();
                            } else {
                                query_msg.edit(ctx, |m| m.embed(|e| {
                                e.description("So, are you a free agent?\n\n(NOTE: If you are currently on a team, picking `Y` will result in you being removed from your team!)")
                                    .color(Color::from_rgb(255, 0, 0))
                            })).await.unwrap();

                                query_msg
                                    .react(
                                        ctx,
                                        channel::ReactionType::Unicode(
                                            emoji::REGIONAL_INDICATOR_Y.into(),
                                        ),
                                    )
                                    .await
                                    .unwrap();
                                query_msg
                                    .react(
                                        ctx,
                                        channel::ReactionType::Unicode(
                                            emoji::REGIONAL_INDICATOR_N.into(),
                                        ),
                                    )
                                    .await
                                    .unwrap();

                                if let Some(input) = query_msg
                                    .await_reaction(ctx)
                                    .removed(false)
                                    .timeout(Duration::from_secs(60))
                                    .author_id(msg.author.id)
                                    .filter(|r| {
                                        if let channel::ReactionType::Unicode(em) = &r.emoji {
                                            em == emoji::REGIONAL_INDICATOR_N
                                                || em == emoji::REGIONAL_INDICATOR_Y
                                        } else {
                                            false
                                        }
                                    })
                                    .await
                                {
                                    if let reaction_collector::ReactionAction::Added(em) =
                                        input.deref()
                                    {
                                        if let channel::ReactionType::Unicode(em) =
                                            &em.deref().emoji
                                        {
                                            match em {
                                                em if em == emoji::REGIONAL_INDICATOR_N => {
                                                    player.set_free_agent(false);
                                                    modified = true;
                                                }
                                                em if em == emoji::REGIONAL_INDICATOR_Y => {
                                                    player.set_free_agent(true);
                                                    modified = true;
                                                }
                                                _ => (),
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        em if em == emoji::EIGHT => {
                            query_msg.edit(ctx, |m| m.embed(|e| {
                                e.description("So, would you like your FC hidden?\n\n(NOTE: If you bring up your own profile, your FC will be show regardless of this setting. This primarily hides your FC from other people bringing up your profile.)")
                                    .color(Color::from_rgb(255, 0, 0))
                            })).await.unwrap();

                            query_msg
                                .react(
                                    ctx,
                                    channel::ReactionType::Unicode(
                                        emoji::REGIONAL_INDICATOR_Y.into(),
                                    ),
                                )
                                .await
                                .unwrap();
                            query_msg
                                .react(
                                    ctx,
                                    channel::ReactionType::Unicode(
                                        emoji::REGIONAL_INDICATOR_N.into(),
                                    ),
                                )
                                .await
                                .unwrap();

                            if let Some(input) = query_msg
                                .await_reaction(ctx)
                                .removed(false)
                                .timeout(Duration::from_secs(60))
                                .author_id(msg.author.id)
                                .filter(|r| {
                                    if let channel::ReactionType::Unicode(em) = &r.emoji {
                                        em == emoji::REGIONAL_INDICATOR_N
                                            || em == emoji::REGIONAL_INDICATOR_Y
                                    } else {
                                        false
                                    }
                                })
                                .await
                            {
                                if let reaction_collector::ReactionAction::Added(em) = input.deref()
                                {
                                    if let channel::ReactionType::Unicode(em) = &em.deref().emoji {
                                        match em {
                                            em if em == emoji::REGIONAL_INDICATOR_N => {
                                                player.is_private = false;
                                                modified = true;
                                            }
                                            em if em == emoji::REGIONAL_INDICATOR_Y => {
                                                player.is_private = true;
                                                modified = true;
                                            }
                                            _ => (),
                                        }
                                    }
                                }
                            }
                        }
                        _ => (),
                    }
                }
            }
        }

        query_msg.delete(ctx).await.unwrap();
    } else {
        update_alt(ctx, msg, args, &mut player, &mut modified).await?;
    }
    if modified {
        if let Err(e) = player.update(&mut c).await {
            error!(
                "Failed to update player profile #{}\n\nError: {:?}",
                player.id(),
                e
            );
            msg.channel_id.say(
                ctx,
                "An internal error has occurred! Contact the developers immediately!",
            );
        } else {
            msg.channel_id
                .say(ctx, "Your profile has been updated.")
                .await
                .unwrap();
        }
    }
    Ok(())
}

/// If args are supplied to the update function, this gets called in place of the standard gui-like
/// command.
async fn update_alt(
    ctx: &Context,
    msg: &Message,
    mut args: Args,
    player: &mut Player,
    modified: &mut bool,
) -> CommandResult {
    let cmd = if let Ok(v) = args.single::<String>() {
        v
    } else {
        msg.channel_id
            .say(ctx, "Command Failed - Invalid field passed.")
            .await
            .unwrap();
        return Ok(());
    };

    macro_rules! next_arg {
        ($type:ty) => {{
            if let Ok(v) = args.single::<$type>() {
                v
            } else {
                msg.channel_id
                    .say(ctx, "Command Failed - Invalid parameter passed.")
                    .await
                    .unwrap();
                return Ok(());
            }
        }};
    }

    match cmd.as_str() {
        "1" | "fc" | "friend-code" => {
            if player.set_fc(next_arg!(String).as_str()).is_ok() {
                *modified = true;
            } else {
                msg.channel_id
                    .say(
                        ctx,
                        "Command Failed - Invalid friend code passed. [Must contain 12 digits]",
                    )
                    .await
                    .unwrap();
                return Ok(());
            }
        }
        "2" | "ign" | "in-game-name" => {
            if player.set_name(next_arg!(String)).is_ok() {
                *modified = true;
            } else {
                msg.channel_id
                    .say(
                        ctx,
                        "Command Failed - Invalid name passed. [Must be 10 digits in length]",
                    )
                    .await
                    .unwrap();
            }
        }
        "3" | "lv" | "level" => {
            player.level = next_arg!(i32);
            *modified = true;
        }
        "4" | "rk" | "rank" => {
            let mode = next_arg!(String);
            let rank = next_arg!(String);

            if let Err(ModelError::InvalidParameter(e)) =
                player.set_rank(mode.as_str(), rank.as_str())
            {
                msg.channel_id
                    .say(ctx, format!("Command Failed - {}", e))
                    .await
                    .unwrap();
                return Ok(());
            }
            *modified = true;
        }
        "5" | "pos" | "position" => {
            let raw_pos = next_arg!(i16);

            match Position::try_from(raw_pos - 1) /* because positions are 0-indexed, and the reactions are 1-indexed. */ {
                Ok(v) => {
                    player.position = v;
                    *modified = true;
                }
                Err(_) => {
                    msg.channel_id
                        .say(ctx, "Command Failed - Invalid position passed.\nValid Positions:\n\
                                   ```\n\
                                   1 - Not Set\n\
                                   2 - Frontline\n\
                                   3 - Midline\n\
                                   4 - Backline\n\
                                   5 - Flex\n\
                                   ```")
                        .await
                        .unwrap();
                    return Ok(());
                }
            }
        }
        "6" | "ld" | "loadout" => {
            let ldink_link = next_arg!(String);
            if !ldink_link.starts_with("https://selicia.github.io/en_US/#") {
                let _ = msg.channel_id.say(
            ctx,
            "Command Failed - Invalid link passed. Please use `https://selicia.github.io/en_US`.",
        ).await;
                return Ok(());
            }

            let ldink_hex = ldink_link
                .as_str()
                .trim_start_matches("https://selicia.github.io/en_US/#");
            if ldink_hex.len() != 25 {
                let _ = msg.channel_id.say(
            ctx,
            "Command Failed - Invalid link passed. Please use `https://selicia.github.io/en_US`.",
        ).await;
                return Ok(());
            }

            if let Err(e) = player
                .set_loadout(
                    &mut get_pool_from_ctx!(ctx).acquire().await.unwrap(),
                    ldink_hex,
                )
                .await
            {
                match e.as_ref() {
                    ModelError::NotFound(item, _) => {
                        let _ = msg
                            .channel_id
                            .say(
                                ctx,
                                format!("Command Failed - Item not found: `{:?}`", item),
                            )
                            .await;
                        return Ok(());
                    }
                    ModelError::InvalidParameter(s) => {
                        let _ = msg
                            .channel_id
                            .say(
                                ctx,
                                format!("Command Failed - Invalid parameter provided: `{}`", s),
                            )
                            .await;
                        return Ok(());
                    }
                    ModelError::Database(err, bt) => {
                        let _ = msg.channel_id.say(ctx, "Command Failed - An unexpected error occured! Contact the developers immediately!").await;
                        error!(
                    "Something went sideways with the database!\n\nError: {:?}\n\nBacktrace: {:#?}",
                    err, bt
                );
                        return Ok(());
                    }
                    ModelError::Unknown(err, bt) => {
                        let _ = msg.channel_id.say(ctx, "Command Failed - An unexpected error occured! Contact the developers immediately!").await;
                        error!(
                            "Something went sideways!\n\nError: {:?}\n\nBacktrace: {:#?}",
                            err, bt
                        );
                        return Ok(());
                    }
                    _ => unreachable!(),
                }
            } else {
                *modified = true;
            }
        }
        "7" | "fa" | "free-agent" => {
            let stat = match next_arg!(String).as_str() {
                "y" | "yes" | "1" => true,
                "n" | "no" | "0" => false,
                _ => {
                    msg.channel_id
                        .say(ctx, "Command Failed - Invalid parameter passed. [Y/N only]")
                        .await
                        .unwrap();
                    return Ok(());
                }
            };
            player.set_free_agent(stat);
            *modified = true;
        }
        "8" | "pri" | "private" => {
            let stat = match next_arg!(String).as_str() {
                "y" | "yes" | "1" => true,
                "n" | "no" | "0" => false,
                _ => {
                    msg.channel_id
                        .say(ctx, "Command Failed - Invalid parameter passed. [Y/N only]")
                        .await
                        .unwrap();
                    return Ok(());
                }
            };
            player.is_private = stat;
            *modified = true;
        }
        _ => {
            msg.channel_id
                .say(ctx, "Command Failed - Invalid parameter passed.")
                .await
                .unwrap();
            return Ok(());
        }
    }

    Ok(())
}

async fn update_set_rank(
    ctx: &Context,
    msg: &Message,
    query_msg: &mut Message,
    player: &mut Player,
) -> bool {
    loop {
        query_msg.delete_reactions(ctx).await.unwrap();

        query_msg
            .edit(ctx, |m| {
                m.embed(|e| {
                    e.description(
                        "What rank do you want to set?\n\
                    `SZ` : Splat Zones\n\
                    `TC` : Tower Control\n\
                    `RM` : Rainmaker\n\
                    `CB` : Clam Blitz\n\
                    `SR` : Salmon Run\n\
                    (Send `quit` to cancel)
                    ",
                    )
                    .color(Color::from_rgb(255, 0, 0))
                })
            })
            .await
            .unwrap();

        let mode = if let Some(resp) = msg
            .author
            .await_reply(ctx)
            .timeout(Duration::from_secs(60))
            .channel_id(msg.channel_id)
            .await
        {
            if !(resp.content.to_ascii_lowercase() == "quit") {
                resp.delete(ctx).await.unwrap();
                resp.content.to_ascii_lowercase().clone()
            } else {
                return false;
            }
        } else {
            return false;
        };

        query_msg
            .edit(ctx, |m| {
                m.embed(|e| {
                    e.description("What's your rank in that mode? (Send `quit` to cancel)")
                        .color(Color::from_rgb(255, 0, 0))
                })
            })
            .await
            .unwrap();

        let rank = if let Some(resp) = msg
            .author
            .await_reply(ctx)
            .timeout(Duration::from_secs(60))
            .channel_id(msg.channel_id)
            .await
        {
            if !(resp.content.to_ascii_lowercase() == "quit") {
                resp.delete(ctx).await.unwrap();
                resp.content.to_ascii_uppercase().clone()
            } else {
                return false;
            }
        } else {
            return false;
        };

        match player.set_rank(&mode, &rank) {
            Ok(()) => return true,
            Err(ModelError::InvalidParameter(err)) => {
                query_msg
                    .edit(ctx, |m| {
                        m.embed(|e| e.description(format!("{}. Try again.", err)))
                    })
                    .await
                    .unwrap();
                tokio::time::delay_for(Duration::from_secs(3)).await;
                continue;
            }
            Err(e) => unreachable!(),
        }
    }
    false
}

#[command("invites")]
#[aliases("inbox", "inv")]
#[description("View invites sent to you by team captains seeking to recruit you to their team.")]
pub async fn player_invites(ctx: &Context, msg: &Message) -> CommandResult {
    let player = get_player!(ctx, msg, *msg.author.id.as_u64());
    let mut c = &mut get_pool_from_ctx!(ctx).acquire().await.unwrap();
    let invite_list: Vec<TeamInvite> = match TeamInvite::fetch_by_player(&mut *c, &player).await {
        Ok(v) => v,
        Err(e) => {
            error!(
                "Failed to retrieve invites for player #{}\n\nError: {:?}",
                msg.author.id.as_u64(),
                e
            );
            msg.channel_id.say(ctx, "Command Failed - An internal error occurred! Contact the developers immediately!").await.unwrap();
            return Ok(());
        }
    };

    if invite_list.is_empty() {
        msg.channel_id
            .say(ctx, "No invites available.")
            .await
            .unwrap();
        return Ok(());
    }

    let il_len = invite_list.len();

    let mut invite_embeds: Vec<(usize, CreateEmbed, TeamInvite)> = Default::default();

    for (idx, invite) in invite_list.into_iter().enumerate() {
        let mut em = CreateEmbed::default();
        em.title(format!("Invite #{} of {}", idx + 1, il_len))
            .description(format!(
                "MESSAGE FROM THE CAPTAIN:\n{}",
                invite.msg().clone().unwrap_or("N/A".into())
            ))
            .field("Team:", invite.sender().name(), true);
        // Getting the captain tied to this invite (since teams are tied to their captain by their
        // discord ID)
        match ctx.http.get_user(*invite.sender().id()).await {
            Ok(cap) => {
                em.field("Sender:", cap.tag(), true);
            }
            Err(e) => {
                error!(
                    "Failed to retrieve user #{} from the Discord API\n\nError: {:?}",
                    invite.sender().id(),
                    e
                );
                msg.channel_id
                    .say(
                        ctx,
                        "Command Failed - Could not retrieve captain info from the Discord API.",
                    )
                    .await
                    .unwrap();
                return Ok(());
            }
        }

        invite_embeds.push((idx, em, invite));
    }

    let mut invite_display: Message = msg
        .channel_id
        .send_message(ctx, |m| {
            m.embed(|e| {
                *e = invite_embeds[0].1.clone();
                e
            })
            .content(format!(
                "Quick Guide:
{} : First Invite
{} : Previous Invite
{} : Next Invite
{} : Last Invite
{} : Reject Invite
{} : Accept Invite
",
                emoji::STRT_ITEM,
                emoji::PREV_ITEM,
                emoji::NEXT_ITEM,
                emoji::LAST_ITEM,
                emoji::CROSS_MARK,
                emoji::CHECK_MARK
            ))
        })
        .await
        .unwrap();

    invite_display
        .react(ctx, channel::ReactionType::Unicode(emoji::STRT_ITEM.into()))
        .await
        .unwrap();
    invite_display
        .react(ctx, channel::ReactionType::Unicode(emoji::PREV_ITEM.into()))
        .await
        .unwrap();
    invite_display
        .react(ctx, channel::ReactionType::Unicode(emoji::NEXT_ITEM.into()))
        .await
        .unwrap();
    invite_display
        .react(ctx, channel::ReactionType::Unicode(emoji::LAST_ITEM.into()))
        .await
        .unwrap();
    invite_display
        .react(
            ctx,
            channel::ReactionType::Unicode(emoji::CROSS_MARK.into()),
        )
        .await
        .unwrap();
    invite_display
        .react(
            ctx,
            channel::ReactionType::Unicode(emoji::CHECK_MARK.into()),
        )
        .await
        .unwrap();

    let mut current_idx: usize = 0;
    let mut react_cleared = false;

    'disp: while let Some(input) = invite_display
        .await_reaction(ctx)
        .author_id(msg.author.id)
        .filter(|r| {
            if let serenity::model::channel::ReactionType::Unicode(em) = &r.emoji {
                em == emoji::STRT_ITEM
                    || em == emoji::PREV_ITEM
                    || em == emoji::NEXT_ITEM
                    || em == emoji::LAST_ITEM
                    || em == emoji::CROSS_MARK
                    || em == emoji::CHECK_MARK
            } else {
                false
            }
        })
        .await
    {
        if let serenity::collector::ReactionAction::Added(input) = input.deref() {
            if let serenity::model::channel::ReactionType::Unicode(em) = &input.deref().emoji {
                match em {
                    em if em == emoji::STRT_ITEM => {
                        input.delete(ctx).await.unwrap();
                        current_idx = 0;
                        invite_display
                            .edit(ctx, |m| {
                                m.embed(|e| {
                                    *e = invite_embeds[0].1.clone();
                                    e
                                })
                            })
                            .await
                            .unwrap();
                    }
                    em if em == emoji::LAST_ITEM => {
                        input.delete(ctx).await.unwrap();
                        current_idx = invite_embeds.len() - 1;
                        invite_display
                            .edit(ctx, |m| {
                                m.embed(|e| {
                                    *e = invite_embeds[invite_embeds.len() - 1].1.clone();
                                    e
                                })
                            })
                            .await
                            .unwrap();
                    }
                    em if em == emoji::PREV_ITEM => {
                        input.delete(ctx).await.unwrap();
                        current_idx = if current_idx == 0 {
                            invite_embeds.len() - 1
                        } else {
                            current_idx - 1
                        };
                        invite_display
                            .edit(ctx, |m| {
                                m.embed(|e| {
                                    *e = invite_embeds[current_idx].1.clone();
                                    e
                                })
                            })
                            .await
                            .unwrap();
                    }
                    em if em == emoji::NEXT_ITEM => {
                        input.delete(ctx).await.unwrap();
                        current_idx = if current_idx == invite_embeds.len() - 1 {
                            0
                        } else {
                            current_idx + 1
                        };
                        invite_display
                            .edit(ctx, |m| {
                                m.embed(|e| {
                                    *e = invite_embeds[current_idx].1.clone();
                                    e
                                })
                            })
                            .await
                            .unwrap();
                    }
                    em if em == emoji::CROSS_MARK => {
                        invite_display.delete_reactions(ctx).await.unwrap();
                        react_cleared = true;
                        invite_display
                            .edit(ctx, |m| {
                                m.content("Are you sure you want to reject this invite?")
                                    .embed(|e| {
                                        *e = invite_embeds[current_idx].1.clone();
                                        e
                                    })
                            })
                            .await
                            .unwrap();
                        invite_display
                            .react(
                                ctx,
                                channel::ReactionType::Unicode(emoji::CROSS_MARK.into()),
                            )
                            .await
                            .unwrap();
                        invite_display
                            .react(
                                ctx,
                                channel::ReactionType::Unicode(emoji::CHECK_MARK.into()),
                            )
                            .await
                            .unwrap();
                        if let Some(r) = invite_display
                            .await_reaction(ctx)
                            .author_id(msg.author.id)
                            .timeout(Duration::from_secs(60))
                            .filter(|r| {
                                if let serenity::model::channel::ReactionType::Unicode(em) =
                                    &r.emoji
                                {
                                    em == emoji::CROSS_MARK || em == emoji::CHECK_MARK
                                } else {
                                    false
                                }
                            })
                            .await
                        {
                            if let serenity::collector::ReactionAction::Added(r) = r.deref() {
                                if let serenity::model::channel::ReactionType::Unicode(em) =
                                    &r.deref().emoji
                                {
                                    match em.as_str() {
                                        em if em == emoji::CHECK_MARK => {
                                            if let Err(e) = invite_embeds
                                                .remove(current_idx)
                                                .2
                                                .delete(&mut c)
                                                .await
                                            {
                                                error!(
                                                    "Failed to accept {:?}\n\nError: {:?}",
                                                    invite_embeds[current_idx].2, e
                                                );
                                                msg.channel_id.say(ctx, "Command Failed - An internal error has occurred! Contact the developers immediately!").await.unwrap();
                                                return Ok(());
                                            } else {
                                                invite_display.delete(ctx).await.unwrap();
                                                msg.channel_id
                                                    .say(ctx, "The invite has been deleted.")
                                                    .await
                                                    .unwrap();
                                                return Ok(());
                                            }
                                        }
                                        em if em == emoji::CROSS_MARK => {
                                            invite_display
                                                .edit(ctx, |m| {
                                                    m.embed(|e| {
                                                        *e = invite_embeds[current_idx].1.clone();
                                                        e
                                                    })
                                                    .content("")
                                                })
                                                .await
                                                .unwrap();
                                            invite_display.delete_reactions(ctx).await.unwrap();
                                        }
                                        _ => (),
                                    }
                                }
                            }
                        }
                    }
                    em if em == emoji::CHECK_MARK => {
                        invite_display.delete_reactions(ctx).await.unwrap();
                        react_cleared = true;
                        invite_display
                            .edit(ctx, |m| {
                                m.content("Are you sure you want to accept this invite? This will unset your FA status, clear all other invites, and update your profile with your Team ID, so consider this *very* carefully.")
                                    .embed(|e| {
                                        *e = invite_embeds[current_idx].1.clone();
                                        e
                                    })
                            })
                            .await
                            .unwrap();
                        invite_display
                            .react(
                                ctx,
                                channel::ReactionType::Unicode(emoji::CROSS_MARK.into()),
                            )
                            .await
                            .unwrap();
                        invite_display
                            .react(
                                ctx,
                                channel::ReactionType::Unicode(emoji::CHECK_MARK.into()),
                            )
                            .await
                            .unwrap();
                        if let Some(r) = invite_display
                            .await_reaction(ctx)
                            .author_id(msg.author.id)
                            .timeout(Duration::from_secs(60))
                            .filter(|r| {
                                if let serenity::model::channel::ReactionType::Unicode(em) =
                                    &r.emoji
                                {
                                    em == emoji::CROSS_MARK || em == emoji::CHECK_MARK
                                } else {
                                    false
                                }
                            })
                            .await
                        {
                            if let serenity::collector::ReactionAction::Added(r) = r.deref() {
                                if let serenity::model::channel::ReactionType::Unicode(em) =
                                    &r.deref().emoji
                                {
                                    match em.as_str() {
                                        em if em == emoji::CHECK_MARK => {
                                            if let Err(e) = invite_embeds
                                                .remove(current_idx)
                                                .2
                                                .accept(&mut *c)
                                                .await
                                            {
                                                error!(
                                                    "Failed to accept {:?}\n\nError: {:?}",
                                                    invite_embeds[current_idx].2, e
                                                );
                                                msg.channel_id.say(ctx, "Command Failed - An internal error has occurred! Contact the developers immediately!").await.unwrap();
                                                return Ok(());
                                            } else {
                                                invite_display.delete(ctx).await.unwrap();
                                                for (_, _, invite) in invite_embeds.into_iter() {
                                                    if let Err(e) = invite.delete(&mut *c).await {
                                                        error!("Failed to clear invites for user {:?}\n\nError: {:?}", msg.author.id.as_u64(), e);
                                                        msg.channel_id.say(ctx, "Command Failed - An internal error has occurred! Contact the developers immediately!").await.unwrap();
                                                        return Ok(());
                                                    }
                                                }
                                                msg.channel_id.say(ctx, "The invite has been accepted. Congratulations.").await.unwrap();
                                                return Ok(());
                                            }
                                        }
                                        em if em == emoji::CROSS_MARK => {
                                            invite_display
                                                .edit(ctx, |m| {
                                                    m.embed(|e| {
                                                        *e = invite_embeds[current_idx].1.clone();
                                                        e
                                                    })
                                                    .content("")
                                                })
                                                .await
                                                .unwrap();
                                            invite_display.delete_reactions(ctx).await.unwrap();
                                        }
                                        _ => (),
                                    }
                                }
                            }
                        }
                    }
                    _ => (),
                }
                if react_cleared {
                    invite_display
                        .react(ctx, channel::ReactionType::Unicode(emoji::STRT_ITEM.into()))
                        .await
                        .unwrap();
                    invite_display
                        .react(ctx, channel::ReactionType::Unicode(emoji::PREV_ITEM.into()))
                        .await
                        .unwrap();
                    invite_display
                        .react(ctx, channel::ReactionType::Unicode(emoji::NEXT_ITEM.into()))
                        .await
                        .unwrap();
                    invite_display
                        .react(ctx, channel::ReactionType::Unicode(emoji::LAST_ITEM.into()))
                        .await
                        .unwrap();
                    invite_display
                        .react(
                            ctx,
                            channel::ReactionType::Unicode(emoji::CROSS_MARK.into()),
                        )
                        .await
                        .unwrap();
                    invite_display
                        .react(
                            ctx,
                            channel::ReactionType::Unicode(emoji::CHECK_MARK.into()),
                        )
                        .await
                        .unwrap();
                    react_cleared = false;
                }
            }
        }
    }
    // screw it, try to delete the message and just move on if it fails. we've done what we need to
    // anyways.
    let _ = invite_display.delete(ctx).await;

    Ok(())
}

#[command("show")]
#[aliases("profile", "info")]
#[usage("[PLAYER_ID or @MENTION]")]
#[example("7C05D0684BBA9C49")]
#[example("KillinTime#5657")]
#[description("Show a player's profile. If an ID or @ mention is provided, it will show the profile of the specified user if one exists. Otherwise, the profile of the command's user will be shown.")]
pub async fn player_show(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let mut self_retrieve: bool = false;
    let player_id: u64 = if args.is_empty() {
        self_retrieve = true;
        *msg.author.id.as_u64()
    } else if msg.mentions.len() == 1 {
        *msg.mentions[0].id.as_u64()
    } else if let Ok(v) = args.single::<u64>() {
        v
    } else {
        let _ = msg
            .channel_id
            .say(ctx, "Command Failed - Invalid argument passed.")
            .await;
        return Ok(());
    };
    let mut c = get_pool_from_ctx!(ctx).acquire().await.unwrap();
    let player: Player = match Player::from_db(&mut c, player_id).await {
        Ok(v) => v,
        Err(e) => {
            if let ModelError::NotFound(kind, trace) = e.as_ref() {
                if let NFKind::Player(_) = kind {
                    let notif = if self_retrieve {
                        "Command Failed - You're not in the database! Add yourself with `player new`.".to_string()
                    } else {
                        format!(
                            "Command Failed - Player ID {} is not in the database.",
                            player_id
                        )
                    };

                    let _ = msg.channel_id.say(ctx, notif).await;
                    return Ok(());
                } else {
                    let _ = msg.channel_id.say(
                        ctx,
                        "Command Failed - An error occured! Contact the developers immediately!",
                    ).await;
                    error!(
                        "Something went sideways!\n\nError: {:?}\n\nBacktrace: {:#?}",
                        e, trace
                    );
                    return Ok(());
                }
            } else {
                let _ = msg
                    .channel_id
                    .say(
                        ctx,
                        "Command Failed - An error occured! Contact the developers immediately!",
                    )
                    .await;
                error!("Something went sideways!\n\nError: {:#?}", e);
                return Ok(());
            }
        }
    };

    let i = player.loadout().to_img().unwrap().to_rgba();
    let w = i.width();
    let h = i.height();

    let mut buf: Vec<u8> = Vec::new();

    let encoder = PNGEncoder::new(&mut buf);
    encoder
        .encode(&i.into_raw(), w, h, ColorType::RGBA(8))
        .unwrap();

    let usr = match ctx.http.get_user(player_id).await {
        Ok(v) => v,
        Err(e) => {
            error!(
                "Failed to retrieve user with ID #{} from Discord API\n\nError: {:?}",
                player.id(),
                e
            );
            msg.channel_id
                .say(
                    ctx,
                    format!("Command Failed - Discord User `{}` not found.", player.id()),
                )
                .await
                .unwrap();
            return Ok(());
        }
    };
    let url = ctx.http.as_ref()
        .get_current_user().await.unwrap()
        .avatar_url().unwrap_or_else( ||
            "https://cdn.discordapp.com/attachments/637014853386764338/646812631130177546/JPEG_20190504_150808.png"
            .to_string()
            );

    let team_name = if let Some(v) = player.team_id() {
        match Team::name_only(&mut c, *v).await {
            Ok(v) => v,
            Err(e) => {
                error!("Failed to retrieve name for team {}\n\nError: {:?}", v, e);
                /// Just return a small error for now, just so players can still access their
                /// profiles normally.
                Some("`[ERR_TEAM_RETRIEVAL_FAILED]`".into())
            }
        }
    } else {
        None
    };

    msg.channel_id.send_message(ctx, |m| {
        m.embed(|e| {
            e.title(format!("Player Profile - {}", usr.name));
            e.field("In-Game Name", player.name(), true);
            e.field("Friend Code", if player.is_private {
                if *msg.author.id.as_u64() == *player.id() {
                    player.fc()
                } else {
                    "SW-////-////-////"
                }
            } else {
                player.fc()
            }, true);
            e.field("Level", player.level, true);
            e.field("Position", player.position, true);

            for (mode, rank) in &player.ranks() {
                e.field(mode, rank, true);
            }
            e.image("attachment://ld.png");
                e.thumbnail(usr.face());
            if *player.is_free_agent() {
                e.color(Color::from_rgb(0xFF, 0x4F, 0x00));
                e.footer(|f| {
                    f.text("This user is a free agent! Spread the word and help this person get a team!");
                    f.icon_url(url)
                });
            } else {
                e.color(Color::from_rgb(0xFF, 0x00, 0x00));
            }
            e
        });
        m.add_file(AttachmentType::Bytes {data: std::borrow::Cow::from(&buf), filename: "ld.png".to_string()});
        m
    }).await.unwrap();

    Ok(())
}
