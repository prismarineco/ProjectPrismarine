use crate::get_pool_from_ctx;
use crate::utils::db::server_cfg;
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::prelude::*,
    prelude::*,
};

#[command]
#[required_permissions(MANAGE_GUILD)]
#[aliases("set_fix", "sf")]
#[usage("set_prefix [PREFIX]")]
#[example("set_prefix prx.")]
#[description("Set a server's custom prefix. If the `PREFIX` argument is left empty, then the custom prefix for the server will be removed.")]
pub async fn set_prefix(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let mut conn = get_pool_from_ctx!(ctx).acquire().await.unwrap();
    let server_id = msg.guild_id.unwrap();
    let server_id = server_id.as_u64();

    if args.is_empty() {
        if let Err(e) = server_cfg::drop_server_prefix(&mut conn, *server_id).await {
            error!(
                "Failed to update prefix for server #{}\n\nError: {:?}",
                server_id, e
            );
            msg.channel_id.say(&ctx, "Command Failed - An internal error occurred! Contact the developers immediately!").await.unwrap();
        } else {
            msg.channel_id
                .say(&ctx, "Reset your server's prefix.")
                .await
                .unwrap();
        }
    } else {
        let new_prefix = args.single::<String>().unwrap();
        if let Err(e) =
            server_cfg::set_server_prefix(&mut conn, *server_id, Some(&new_prefix)).await
        {
            error!(
                "Failed to update prefix for guild #{}\n\nError: {:?}",
                server_id, e
            );
            msg.channel_id.say(&ctx, "Command Failed - An internal error occurred! Contact the developers immediately!").await.unwrap();
        } else {
            msg.channel_id
                .say(&ctx, format!("Set your prefix to `{}`.", new_prefix))
                .await
                .unwrap();
        }
    }

    Ok(())
}

#[command]
#[required_permissions(MANAGE_GUILD)]
#[aliases("whitelist", "wl")]
#[usage("whitelist_channel [LIST_OF_CHANNELS...]")]
#[example("cfg whitelist #foo #bar")]
#[description("This command will whitelist a specific channel for listening to commands. By default, it whitelists the channel it was invoked in, otherwise it will whitelist all mentioned channels. If a command is invoked outside these whitelisted channels, it will not run. If a server's whitelist is empty, then commands will be allowed in all channels, otherwise only whitelisted channels will be allowed to have commands invoked within them.\n\nOnly server members with the `ADMINISTRATOR` permission are allowed to bypass channel restrictions, and you must have the `MANAGE_GUILD` permission to whitelist channels.")]
pub async fn whitelist_channel(ctx: &Context, msg: &Message) -> CommandResult {
    let mut conn = get_pool_from_ctx!(ctx).acquire().await.unwrap();
    let server_id = msg.guild_id.unwrap();
    let server_id = server_id.as_u64();

    let mut channel_ids: Vec<u64> = vec![];

    if let Some(channels) = &msg.mention_channels {
        println!("{:?}", channels);
        for channel in channels.iter() {
            channel_ids.push(*channel.id.as_u64());
        }
    } else {
        channel_ids.push(*msg.channel_id.as_u64());
    }

    for id in channel_ids.iter() {
        println!("{}, {}", id, server_id);
        if let Err(e) = server_cfg::whitelist_channel(&mut conn, *server_id, *id).await {
            error!(
                "Failed to add channel #{} in server #{} to whitelist\n\nError: {:?}",
                id, server_id, e
            );
            msg.channel_id.say(&ctx, "Command Failed - An internal error occurred! Contact the developers immediately!").await.unwrap();
            return Ok(());
        }
    }

    msg.channel_id
        .say(&ctx, "Added all specified channels to whitelist.")
        .await
        .unwrap();

    Ok(())
}

#[command]
#[required_permissions(MANAGE_GUILD)]
#[aliases("unwhitelist", "uwl")]
#[usage("unwhitelist_channel [LIST_OF_CHANNELS...]")]
#[description("Remove a channel from a server's whitelist. By default, it removes the channel it was invoked in, otherwise it will remove all mentioned channels from a server's blacklist. This takes the same arguments as `whitelist_channel`.")]
pub async fn unwhitelist_channel(ctx: &Context, msg: &Message) -> CommandResult {
    let mut conn = get_pool_from_ctx!(ctx).acquire().await.unwrap();
    let server_id = msg.guild_id.unwrap();
    let server_id = *server_id.as_u64();

    if let Some(channels) = &msg.mention_channels {
        for channel in channels.iter() {
            if let Err(e) =
                server_cfg::unwhitelist_channel(&mut conn, server_id, *channel.id.as_u64()).await
            {
                error!(
                    "Failed to remove channel #{} from server #{}'s whitelist\n\nError: {:?}",
                    channel.id.as_u64(),
                    server_id,
                    e
                );
                msg.channel_id.say(&ctx, "Command Failed - An internal error occurred! Contact the developers immediately!").await.unwrap();
            }
        }
    } else {
        if let Err(e) =
            server_cfg::unwhitelist_channel(&mut conn, server_id, *msg.channel_id.as_u64()).await
        {
            error!(
                "Failed to remove channel #{} from server #{}'s whitelist\n\nError: {:?}",
                msg.channel_id.as_u64(),
                server_id,
                e
            );
            msg.channel_id.say(&ctx, "Command Failed - An internal error occurred! Contact the developers immediately!").await.unwrap();
        }
    }
    msg.channel_id
        .say(&ctx, "Removed all specified channels to whitelist.")
        .await
        .unwrap();

    Ok(())
}

#[command]
#[aliases("clear", "c")]
#[usage("clear_whitelist")]
#[description("Clear a server's whitelist. This command takes no arguments.")]
pub async fn clear_whitelist(ctx: &Context, msg: &Message) -> CommandResult {
    let mut conn = get_pool_from_ctx!(ctx).acquire().await.unwrap();
    let server_id = msg.guild_id.unwrap();
    let server_id = *server_id.as_u64();

    if let Err(e) = server_cfg::clear_whitelist(&mut conn, server_id).await {
        error!(
            "Failed to clear server #{}'s whitelist\n\nError: {:?}",
            server_id, e
        );
        msg.channel_id
            .say(
                &ctx,
                "Command Failed - An internal error occurred! Contact the developers immediately!",
            )
            .await
            .unwrap();
    } else {
        msg.channel_id
            .say(&ctx, "Server whitelist cleared.")
            .await
            .unwrap();
    }
    Ok(())
}

#[command]
#[aliases("prefix")]
#[usage("prefix")]
#[description("Show the server's custom prefix.")]
pub async fn show_prefix(ctx: &Context, msg: &Message) -> CommandResult {
    let mut conn = get_pool_from_ctx!(ctx).acquire().await.unwrap();
    let server_id = msg.guild_id.unwrap();
    let server_id = *server_id.as_u64();

    let _ = match server_cfg::get_server_prefix(&mut conn, server_id).await {
        Ok(None) => msg
            .channel_id
            .say(&ctx, "No custom prefix set.")
            .await
            .unwrap(),
        Ok(Some(v)) => msg
            .channel_id
            .say(
                &ctx,
                format!("This server's custom prefix is set to `{}`", v),
            )
            .await
            .unwrap(),
        Err(e) => {
            error!(
                "Failed to retrieve prefix for guild #{}\n\nError: {:?}",
                server_id, e
            );
            msg.channel_id.say(&ctx, "Command Failed - An internal error occurred! Contact the developers immediately!").await.unwrap()
        }
    };

    Ok(())
}
