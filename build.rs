extern crate toml;
use std::io::Read;

#[derive(serde::Deserialize)]
struct Prisbot {
    prisbot: Config,
}

#[derive(serde::Deserialize)]
struct Config {
    database: Database,
}

#[derive(serde::Deserialize)]
struct Database {
    url: String,
}

fn main() {
    let mut f = String::new();
    std::fs::File::open("prisbot.toml")
        .expect("Expected config file named 'prisbot.toml'")
        .read_to_string(&mut f)
        .unwrap();
    let conf: Prisbot = toml::from_str(&f).expect("Invalid TOML config");
    println!("cargo:rustc-env=DATABASE_URL={}", conf.prisbot.database.url);
}
